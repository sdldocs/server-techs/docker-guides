## 이미지 레이어링
이미지를 구성하는 요소를 살펴볼 수 있다는 사실을 알고 있었나요? `docker image history` 명령을 사용하면 이미지 내의 각 레이어를 만드는 데 사용된 명령을 볼 수 있다.

<span>1.</span> 튜토리얼 앞부분에서 만든 `getting-started` 이미지의 레이어를 보려면 `docker image history` 명령을 사용하세요.

```bash
$ docker image history getting-started
```

다음과 같은 출력이 표시되어야 한다(날짜/ID가 다를 수 있음).

```
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
a78a40cbf866        18 seconds ago      /bin/sh -c #(nop)  CMD ["node" "src/index.j…    0B                  
f1d1808565d6        19 seconds ago      /bin/sh -c yarn install --production            85.4MB              
a2c054d14948        36 seconds ago      /bin/sh -c #(nop) COPY dir:5dc710ad87c789593…   198kB               
9577ae713121        37 seconds ago      /bin/sh -c #(nop) WORKDIR /app                  0B                  
b95baba1cfdb        13 days ago         /bin/sh -c #(nop)  CMD ["node"]                 0B                  
<missing>           13 days ago         /bin/sh -c #(nop)  ENTRYPOINT ["docker-entry…   0B                  
<missing>           13 days ago         /bin/sh -c #(nop) COPY file:238737301d473041…   116B                
<missing>           13 days ago         /bin/sh -c apk add --no-cache --virtual .bui…   5.35MB              
<missing>           13 days ago         /bin/sh -c #(nop)  ENV YARN_VERSION=1.21.1      0B                  
<missing>           13 days ago         /bin/sh -c addgroup -g 1000 node     && addu…   74.3MB              
<missing>           13 days ago         /bin/sh -c #(nop)  ENV NODE_VERSION=12.14.1     0B                  
<missing>           13 days ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B                  
<missing>           13 days ago         /bin/sh -c #(nop) ADD file:e69d441d729412d24…   5.59MB
```

각 행은 이미지의 레이어를 나타낸다. 여기 출력에는 하단에 기본 레이어가 표시되고 상단에 최신 레이어가 표시된다. 이를 사용하면 각 레이어의 크기를 빠르게 확인할 수 있으므로 큰 이미지를 진단하는 데 도움이 된다.

<span>2.</span> 여러 행이 잘린 것을 볼 수 있다. `--no-trunc` 플래그를 추가하면 전체 출력을 얻을 수 있다(네... 잘림 방지 플래그를 사용하여 잘리지 않은 출력을 얻는 방법이 재미있죠?).

```bash
docker image history --no-trunc getting-started
```

## 레이어 캐싱
이제 레이어링이 실제로 작동하는 것을 보았으니 컨테이너 이미지의 빌드 시간을 단축하는 데 도움이 되는 중요한 교훈이 있다.

> 레이어가 변경되면 모든 다운스트림 레이어도 다시 만들어야 한다.

우리가 사용하던 Dockerfile을 다시 한 번 살펴보자...

```dockerfile
# syntax=docker/dockerfile:1
FROM node:18-alpine
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
```

이미지 히스토리 출력으로 돌아가서, Dockerfile의 각 명령이 이미지에서 새 레이어가 되는 것을 볼 수 있다. 이미지를 변경할 때 yarn 종속성을 다시 설치해야 했던 것을 기억할 것이다. 이 문제를 해결할 수 있는 방법이 있을까? 빌드할 때마다 동일한 종속 요소를 배포하는 것은 이치에 어긋나지 않지요?

이 문제를 해결하려면 종속성 캐싱을 지원하도록 Docker파일을 재구성해야 한다. Node 기반 어플리케이션의 경우 이러한 종속성은 `package.json` 파일에 정의되어 있다. 그렇다면 먼저 해당 파일만 복사하고 종속 요소를 설치한 다음 다른 모든 파일을 복사하면 어떨까? 그런 다음 `package.json`에 변경이 있는 경우에만 yarn 종속성을 다시 생성한다. 이해가 되나요?

<span>1.</span> `package.json`을 먼저 복사하고, 종속 요소를 설치한 다음, 다른 모든 것을 복사하도록 Dockerfile을 업데이트하세요.

```dockerfile
# syntax=docker/dockerfile:1
FROM node:18-alpine
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --production
COPY . .
CMD ["node", "src/index.js"]
```

<span>2.</span> Dockerfile과 같은 폴더에 다음 내용으로 `.dockerignore`라는 파일을 만든다.

```
node_modules
```

`.dockerignore` 파일은 이미지 관련 파일만 선택적으로 복사할 수 있는 쉬운 방법이다. 이에 대한 자세한 내용은 [여기](https://docs.docker.com/engine/reference/builder/#dockerignore-file)에서 확인할 수 있다. 이 경우 두 번째 `COPY` 단계에서 `node_modules` 폴더를 생략해야 하는데, 그렇지 않으면 `RUN` 단계의 명령으로 생성된 파일을 덮어쓸 수 있기 때문이다. Node.js 어플리케이션에 이 방법을 권장하는 이유와 기타 모범 사례에 대한 자세한 내용은 [Dockerizing a Node.js web app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)을 참조하세요.

<span>3.</span> `docker build`를 사용하여 새 이미지를 빌드한다.

```bash
$ docker build -t getting-started .
```

다음과 같은 출력이 표시되어야 한다...

```
[+] Building 16.1s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 175B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/node:18-alpine
 => [internal] load build context
 => => transferring context: 53.37MB
 => [1/5] FROM docker.io/library/node:18-alpine
 => CACHED [2/5] WORKDIR /app
 => [3/5] COPY package.json yarn.lock ./
 => [4/5] RUN yarn install --production
 => [5/5] COPY . .
 => exporting to image
 => => exporting layers
 => => writing image     sha256:d6f819013566c54c50124ed94d5e66c452325327217f4f04399b45f94e37d25
 => => naming to docker.io/library/getting-started
```

모든 레이어가 다시 빌드된 것을 볼 수 있다. Dockerfile을 꽤 많이 변경했기 때문에 완벽하게 정상이다.

<span>4.</span> 이제 `src/static/index.html` 파일을 변경한다(예: `<title>`을 "he Awesome Todo App"으로 변경).

<span>5.</span> 이제 `docker build -t getting-started .`를 사용하여 Docker 이미지를 다시 빌드한다. 이번에는 출력이 조금 달라졌을 것이다.

```
[+] Building 1.2s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 37B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/node:18-alpine
 => [internal] load build context
 => => transferring context: 450.43kB
 => [1/5] FROM docker.io/library/node:18-alpine
 => CACHED [2/5] WORKDIR /app
 => CACHED [3/5] COPY package.json yarn.lock ./
 => CACHED [4/5] RUN yarn install --production
 => [5/5] COPY . .
 => exporting to image
 => => exporting layers
 => => writing image     sha256:91790c87bcb096a83c2bd4eb512bc8b134c757cda0bdee4038187f98148e2eda
 => => naming to docker.io/library/getting-started
```

우선, 빌드가 훨씬 빨라진 것을 확인할 수 있다! 그리고 여러 단계에서 이전에 캐시된 레이어를 사용하는 것을 볼 수 있다. 만세! 빌드 캐시를 사용하고 있다. 이 이미지를 밀고 당기는 작업과 업데이트 작업도 훨씬 빨라질 것dl다. 만세!

## 다단계 빌드
이 튜토리얼에서 자세히 다루지는 않겠지만, 다단계 빌드는 여러 단계를 사용하여 이미지를 만드는 데 도움이 되는 매우 강력한 도구이다. 여기에는 몇 가지 장점이 있다.

- 런타임 종속성으로 부터 빌드 시간 종속성을 분리
- 앱 실행에 필요한 것*만* 전송하여 전체 이미지 크기 축소

### Maven과 Tomcat 예
Java 기반 애플리케이션을 빌드할 때 소스 코드를 Java 바이트코드로 컴파일하려면 JDK가 필요하다. 하지만 프로덕션 환경에서는 해당 JDK가 필요하지 않다. 또한 앱 빌드를 돕기 위해 Maven이나 Gradle과 같은 도구를 사용할 수도 있다. 이 역시 최종 이미지에는 필요하지 않다. 다단계 빌드가 도움이 될 것이다.

```dockerfile
# syntax=docker/dockerfile:1
FROM maven AS build
WORKDIR /app
COPY . .
RUN mvn package

FROM tomcat
COPY --from=build /app/target/file.war /usr/local/tomcat/webapps
```

이 예에서는 한 단계(`build`라고 함)를 사용하여 Maven을 사용하여 실제 Java 빌드를 수행한다. 두 번째 단계(`FROM tomcat`에서 시작)에서는 `build` 단계의 파일을 복사합니다. 최종 이미지는 생성되는 마지막 단계일 뿐이다(`--target` 플래그를 사용하여 재정의할 수 있음).

### React 예
React 어플리케이션을 빌드할 때 JS 코드(일반적으로 JSX), SASS 스타일시트 등을 정적 HTML, JS, CSS로 컴파일하려면 Node 환경이 필요하다. 서버 측 렌더링을 수행하지 않는다면 프로덕션 빌드에는 Node 환경이 필요하지 않다. 정적 리소스를 정적 nginx 컨테이너로 전송하는 것은 어떨까?

```dockerfile
# syntax=docker/dockerfile:1
FROM node:18 AS build
WORKDIR /app
COPY package* yarn.lock ./
RUN yarn install
COPY public ./public
COPY src ./src
RUN yarn run build

FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
```

여기서는 `node:18` 이미지를 사용하여 빌드(레이어 캐싱 최대화)를 수행한 다음 출력을 nginx 컨테이너에 복사하고 있다. 멋지지 않나요?

## 다음 단계
이미지의 구조를 조금만 이해하면 이미지를 더 빠르게 빌드하고 변경 사항을 더 적게 배포할 수 있다. 이미지를 스캔하면 실행과 배포하는 컨테이너가 안전하다는 확신을 가질 수 있다. 또한 다단계 빌드는 빌드 시간 종속성과 런타임 종속성을 분리하여 전체 이미지 크기를 줄이고 최종 컨테이너 보안을 강화하는 데 도움이 된다.

다음 Part에서는 컨테이너에 대해 계속 학습하는 데 사용할 수 있는 추가 리소스에 대해 알아보겠다.
