[Docker Compose](https://docs.docker.com/compose/)는 멀티 컨테이너 어플리케이션을 정의하고 공유하는 데 도움을 주기 위해 개발된 도구이다. Compose를 사용하면 서비스를 정의하는 YAML 파일을 생성할 수 있으며, 단일 명령으로 모든 것을 스핀업하거나 해체할 수 있다.

Compose를 사용하면 어플리케이션 스택을 파일로 정의하고, 프로젝트 리포지토리의 루트에 보관하며(이제 버전이 제어됨), 다른 사람이 프로젝트에 쉽게 기여할 수 있다는 큰 이점이 있다. 다른 개발자는 리포지토리를 복제하고 앱 작성을 시작하기만 하면 된다. 실제로 현재 GitHub 또는 GitLab에서 이 작업을 수행하는 프로젝트를 많이 볼 수 있다.

그렇다면 어떻게 시작할 수 있을까?

## Docker Compose 
Windows, Mac 또는 Linux용 Docker Desktop을 설치했다면 이미 Docker Compose가 설치되어 있는 것이다! Play-with-Docker 인스턴스에는 Docker Compose도 이미 설치되어 있다.

독립 실행형 Docker 엔진을 설치하였다면 별도의 패키지로 Docker Compose를 설치해야 한다(`Install Compose plugin`를 참조하세요).

설치가 완료되면 다음을 실행하고 버전 정보를 확인할 수 있어야 한다.

```bash
$ docker compose version
```

## Compose 파일 생성
<span>1.</span> `getting-started/app` 폴더의 루트에 `docker-compose.yml`이라는 파일을 생성한다.

<span>2.</span> compose 파일에서 어플리케이션의 일부로 실행할 서비스(또는 컨테이너) 목록을 정의하는 것부터 시작한다.

```yml
services:
```

이제 한 번에 한 서비스씩 compose 파일로 마이그레이션을 시작하겠다.

## 앱 서비스 정의
이 명령은 앱 컨테이너를 정의하는 데 사용했던 명령이라는 것을 기억하세요.

```bash
$ docker run -dp 127.0.0.1:3000:3000 \
  -w /app -v "$(pwd):/app" \
  --network todo-app \
  -e MYSQL_HOST=mysql \
  -e MYSQL_USER=root \
  -e MYSQL_PASSWORD=secret \
  -e MYSQL_DB=todos \
  node:18-alpine \
  sh -c "yarn install && yarn run dev"
```

<span>1.</span> 먼저 서비스 항목과 컨테이너의 이미지를 정의한다. 서비스 이름은 무엇이든 선택할 수 있다. 이름은 자동으로 네트워크 별칭이 되며, MySQL 서비스를 정의할 때 유용하다.

```yml
services:
  app:
    image: node:18-alpine
```

<span>2.</span> 일반적으로 순서에 대한 요구 사항은 없지만 이미지 정의에 가까운 명령을 볼 수 있다. 이제 이 명령을 파일로 옮겨 보겠다.

```yml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"``
```

<span>3.</span> 서비스의 포트를 정의하여 명령의 `-p 127.0.0.1:3000:3000` 부분을 마이그레이션해 보겠다. 여기서는 짧은 구문을 사용하지만 좀 더 자세한 긴 구문도 사용할 수 있다.

```yml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 127.0.0.1:3000:3000
```

<span>4.</span> 다음으로, 작업 디렉터리(`-w /app`)와 볼륨 매핑(`-v "$(pwd):/app"`)을 모두 `working_dir`와 `volumes` 정의를 사용하여 마이그레이션한다. 볼륨에는 `short`과 `long` 구문이 있다.

Docker Compose 볼륨 정의의 한 가지 장점은 현재 디렉터리에서 상대 경로를 사용할 수 있다는 것이다.

```yml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 127.0.0.1:3000:3000
    working_dir: /app
    volumes:
      - ./:/app
```

<span>5.</span> 마지막으로 `environment` 키를 사용하여 환경 변수 정의를 마이그레이션해야 한다.

```yml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 127.0.0.1:3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos
```

### MySQL 서비스 정의
이제 MySQL 서비스를 정의할 차례이다. 해당 컨테이너에 사용한 명령은 다음과 같다.

```bash
docker run -d \
  --network todo-app --network-alias mysql \
  -v todo-mysql-data:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=secret \
  -e MYSQL_DATABASE=todos \
  mysql:8.0
```

<span>1.</span> 먼저 새 서비스를 정의하고 네트워크 별칭을 자동으로 가져올 수 있도록 `mysql`로 이름을 지정한다. 계속해서 사용할 이미지도 지정하겠다.

```yml
services:
  app:
    # The app service definition
  mysql:
    image: mysql:8.0
```

<span>2.</span> 다음으로 볼륨 매핑을 정의하겠다. `docker run`으로 컨테이너를 실행하면 명명된 볼륨이 자동으로 생성된다. 그러나 Compose로 실행할 때는 그렇지 않다. 최상위 `volume:` 섹션에서 볼륨을 정의한 다음 서비스 구성에서 마운트 포인트를 지정해야 한다. 볼륨 이름만 제공하면 기본 옵션이 사용된다. 하지만 [더 많은 옵션을 사용할 수 있다](https://docs.docker.com/compose/compose-file/07-volumes/).

```yml
services:
  app:
    # The app service definition
  mysql:
    image: mysql:8.0
    volumes:
      - todo-mysql-data:/var/lib/mysql

volumes:
  todo-mysql-data:
```

<span>3.</span> 마지막으로 환경 변수를 지정하기만 하면 된다.

```yml
services:
  app:
    # The app service definition
  mysql:
    image: mysql:8.0
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

이 시점에서 전체 `docker-compose.yml`은 다음과 같은 모습이어야 한다.

```yml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 127.0.0.1:3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos

  mysql:
    image: mysql:8.0
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

## 어플리케이션 스택 실행
이제 `docker-compose.yml` 파일이 생겼으니 시작하면 된다!

<span>1.</span> 먼저 app/db의 다른 복사본이 실행되고 있지 않은지 확인한다(`docker ps` 및 `docker rm -f <ids>`).

<span>2.</span> `docker compose up` 명령을 사용하여 어플리케이션 스택을 시작한다. 모든 것을 백그라운드에서 실행하기 위해 `-d` 플래그를 추가한다.

```bash
$ docker compose up -d
```

이 코드를 실행하면 다음과 같은 출력을 볼 수 있다.

```
Creating network "app_default" with the default driver
Creating volume "app_todo-mysql-data" with default driver
Creating app_app_1   ... done
Creating app_mysql_1 ... done
```

볼륨과 네트워크가 생성된 것을 확인할 수 있습니다! 기본적으로 Docker Compose는 어플리케이션 스택을 위해 특별히 네트워크를 자동으로 생성한다(그래서 compose 파일에 네트워크를 정의하지 않았다).

<span>3.</span> `docker compose logs -f` 명령을 사용하여 로그를 살펴보자. 각 서비스의 로그가 하나의 스트림으로 인터리빙되는 것을 볼 수 있다. 이 기능은 타이밍 관련 문제를 확인하려는 경우 매우 유용하다. `-f` 플래그는 로그를 '팔로우'하므로 로그가 생성될 때 실시간 출력을 제공한다.

이미 명령을 실행했다면 다음과 같은 출력을 볼 수 있다.

```
mysql_1  | 2019-10-03T03:07:16.083639Z 0 [Note] mysqld: ready for connections.
mysql_1  | Version: '8.0.31'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server (GPL)
app_1    | Connected to mysql db at host mysql
app_1    | Listening on port 3000
```

서비스 이름은 메시지를 구분하는 데 도움이 되도록 줄의 시작 부분에 표시(종종 색상이 지정됨)된다. 특정 서비스에 대한 로그를 보려면 로그 명령의 끝에 서비스 이름을 추가하면 된다(예: `docker compose logs -f app`).

<span>4.</span> 이제 앱을 열고 실행 중인 앱을 볼 수 있다. 그리고 여기까지! 명령이 하나만 남았다!

## Docker 대시보드에서 앱 스텍 보기
Docker 대시보드를 보면 **app**이라는 그룹이 있는 것을 볼 수 있다. 이것은 Docker Compose의 "프로젝트 이름"이며 컨테이너를 함께 그룹화하는 데 사용된다. 기본적으로 프로젝트 이름은 단순히 `docker-compose.yml`이 위치한 디렉터리의 이름이다.

![](../images/get-started/dashboard-app-project-collapsed.png)

**app** 옆에 있는 공개 화살표를 클릭하면 compose 파일에서 정의한 두 개의 컨테이너가 표시된다. 이름도 `<service-name>-<replica-number>` 패턴을 따르기 때문에 좀 더 설명이 쉽다. 따라서 어떤 컨테이너가 우리 앱이고 어떤 컨테이너가 mysql 데이터베이스인지 매우 쉽게 확인할 수 있다.

![](../images/get-started/dashboard-app-project-expanded.png)

## 모두 해체
모든 것을 제거할 준비가 되었으면 Docker 대시보드에서 전체 앱에 대해 `docker compose down`를 실행하거나 휴지통을 누르면 된다. 컨테이너가 중지되고 네트워크가 제거된다.

> **Warning**
>
> 볼륨 제거하기
> 
> 기본적으로 `docker compose down`을 실행할 때 컴포즈 파일의 명명된 볼륨은 제거되지 않는다. 볼륨을 제거하려면 `--volumes` 플래그를 추가해야 한다.
> 
> 앱 스택을 삭제할 때 Docker 대시보드는 볼륨을 제거하지 **않는다**.

프로젝트가 해체되면 다른 프로젝트로 전환하여 `docker compose up`을 실행하고 해당 프로젝트에 기여할 준비를 하면 된다! 이보다 더 간단할 수는 없다!

## 다음 단계
이 Part에서는 Docker Compose와 멀티서비스 어플리케이션의 정의와 공유를 획기적으로 간소화하는 데 도움이 되는 방법에 대해 알아 보았다. 사용하던 명령을 적절한 작성 형식으로 변환하여 Compose 파일을 만들었다.

이 시점에서 튜토리얼을 마무리할 수 있다. 하지만 지금까지 사용했던 Dockerfile에 큰 문제가 있으므로 이미지 빌드에 대한 몇 가지 모범 사례를 다루도록 한다.
