지금까지는 단일 컨테이너 앱으로 작업해 왔다. 하지만 이제 어플리케이션 스택에 MySQL을 추가하려 한다. 다음과 같은 질문이 자주 발생한다. "MySQL을 어디에서 실행할 것인가? 같은 컨테이너에 설치하나요, 아니면 별도로 실행하나요?" 일반적으로 각 컨테이너는 한 가지 일만 하고 잘 수행해야 한다. 다음은 컨테이너를 별도로 실행해야 하는 몇 가지 이유이다.

- API와 프론트엔드를 데이터베이스와 다르게 확장해야 할 가능성이 높다.
- 별도의 컨테이너를 사용하면 버전과 업데이트 버전을 따로 관리할 수 있다.
- 로컬에서는 데이터베이스에 컨테이너를 사용할 수 있지만, 프로덕션 환경에서는 데이터베이스에 관리형 서비스를 사용하고 싶을 수 있다. 그렇다면 데이터베이스 엔진을 앱과 함께 제공하고 싶지 않을 것이다.
- 여러 프로세스를 실행하려면 프로세스 관리자(컨테이너는 하나의 프로세스만 시작)가 필요하므로 컨테이너 시작과 종료시 복잡성이 추가된다.

그리고 다른 이유도 있다. 따라서 다음 다이어그램과 같이 여러 컨테이너에서 앱을 실행하는 것이 가장 좋다.

![](../images/get-started/multi-app-architecture.png)

## 컨테이너 네트워킹
컨테이너는 기본적으로 격리되어 실행되며 동일한 머신의 다른 프로세스나 컨테이너에 대해 아무것도 알지 못한다는 점을 기억하세요. 그렇다면 어떻게 한 컨테이너가 다른 컨테이너와 대화할 수 있도록 허용할 수 있을까? 정답은 네트워킹이다. 두 컨테이너를 동일한 네트워크에 배치하면 서로 대화할 수 있다.

## MySQL 시작
네트워크에 컨테이너를 배치하는 두 가지 방법이 있다.

- 컨테이너를 시작할 때 네트워크를 할당한다.
- 이미 실행 중인 컨테이너를 네트워크에 연결한다.

다음 단계에서는 먼저 네트워크를 생성한 다음 시작할 때 MySQL 컨테이너를 연결하는 것이다.

<span>1.</span> 네트워크를 생성한다.

```baash
$ docker network create todo-app
```

<span>2.</span> MySQL 컨테이너를 시작하고 네트워크에 연결한다. 또한 데이터베이스가 데이터베이스를 초기화하는 데 사용할 몇 가지 환경 변수를 정의할 것이다. MySQL 환경 변수에 대해 자세히 알아보려면 [MySQL Docker Hub listing](https://hub.docker.com/_/mysql/?_gl=1*1m08oi7*_ga*MjA1ODI4ODk5NS4xNjg5NzQxNjA2*_ga_XJWPQMJYHQ*MTY5MDc2NzkzNS4xOC4xLjE2OTA3Njc5NDEuNTQuMC4w)의 "환경 변수" 섹션을 참조하세요.

```bash
$ docker run -d \
     --network todo-app --network-alias mysql \
     -v todo-mysql-data:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=secret \
     -e MYSQL_DATABASE=todos \
     mysql:8.0
```

위의 명령에서 `--network-alias` 플래그를 볼 수 있다. 이후 섹션에서 이 플래그에 대해 자세히 알아보겠다.

> **Tip**
> 
> 위 명령에서 `todo-mysql-data`라는 이름의 볼륨을 볼 수 있는데, 이 볼륨은 MySQL이 데이터를 저장하는 위치인 `/var/lib/mysql`에 마운트되어 있다. 하지만 `docker volume create` 명령을 실행한 적이 없다. Docker는 사용자가 명명한 볼륨을 사용하려는 것을 인식하고 자동으로 볼륨을 생성한다.

<span>3.</span> 데이터베이스가 실행 중인지 확인하려면 데이터베이스에 연결하여 데이터베이스가 연결되는지 확인할 수 있다.

```bash
$ docker exec -it <mysql-container-id> mysql -u root -p
```

비밀번호 프롬프트가 나타나면 `secret`을 입력한다. MySQL 셸에서 데이터베이스를 나열하고 `todo` 데이터베이스가 있는지 확인한다.

```
mysql> SHOW DATABASES;
```

다음과 같은 출력이 디스플레이된다.

```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| todos              |
+--------------------+
5 rows in set (0.00 sec)
```

<span>4.</span> MySQL 셸을 종료하여 컴퓨터의 셸로 돌아간다.

```
mysql> exit
```

이제 `todos` 데이터베이스가 만들어졌으며 사용할 준비가 되어 있다.

## MySQL에 연결
이제 MySQL이 실행 중이라는 것을 알았으니 사용할 수 있다. 하지만 어떻게 사용하나? 동일한 네트워크에서 다른 컨테이너를 실행하는 경우 컨테이너를 어떻게 찾을 수 있을까? 각 컨테이너에는 고유한 IP 주소가 있다는 것을 기억하세요.

위의 질문에 답하고 컨테이너 네트워킹을 더 잘 이해하기 위해, 네트워킹 문제를 해결하거나 디버깅하는 데 유용한 많은 도구가 함께 제공되는 [nicolaka/netshoot](https://github.com/nicolaka/netshootetshoot) 컨테이너를 사용할 것이다.

<span>1.</span> nicolaka/netshoot 이미지를 사용하여 새 컨테이너를 시작한다. 동일한 네트워크에 연결해야 한다.

```bash
$ docker run -it --network todo-app nicolaka/netshoot
```

<span>2.</span> 컨테이너 내부에서 유용한 DNS 도구인 `dig` 명령을 사용할 것이다. 호스트 이름 `mysql`의 IP 주소를 조회한다.

```bash
$ dig mysql
```

다음과 같은 출력을 얻을 수 있다.

```
; <<>> DiG 9.18.8 <<>> mysql
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32162
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;mysql.				IN	A

;; ANSWER SECTION:
mysql.			600	IN	A	172.23.0.2

;; Query time: 0 msec
;; SERVER: 127.0.0.11#53(127.0.0.11)
;; WHEN: Tue Oct 01 23:47:24 UTC 2019
;; MSG SIZE  rcvd: 44
```

"ANSWER SECTION"에 `172.23.0.2`로 확인되는 `mysql`에 대한 `A` 레코드가 표시됩니다(여러분의 IP 주소는 다른 값을 가질 가능성이 높다). `mysql`은 일반적으로 유효한 호스트 이름이 아니지만, Docker는 해당 네트워크 별칭을 가진 컨테이너의 IP 주소로 확인할 수 있었다. 앞서 `--network-alias`를 사용했음을 기억하세요.

즉, 앱이 `mysql`이라는 호스트에 연결하기만 하면 데이터베이스와 통신할 수 있다는 뜻이다.

## MySQL과 앱 
todo 앱은 MySQL 연결 설정을 지정하기 위한 다음과 같은 몇 가지 환경 변수 설정을 지원한다.

- `MYSQL_HOST`: 실행 중인 MySQL 서버의 호스트 이름
- `MYSQL_USER`: 연결에 사용할 사용자 이름
- `MYSQL_PASSWORD`: 연결에 사용할 비밀번호
- `MYSQL_DB`: 연결 후 사용할 데이터베이스

> **Note**:
>
> 환경 변수를 사용하여 연결 설정을 설정하는 것은 일반적으로 개발용으로 허용되지만, 프로덕션 환경에서 어플리케이션을 실행할 때는 사용하지 않는 것이 좋다. Docker의 전 보안 책임자였던 Diogo Monica는 그 이유를 설명하는 [멋진 블로그 게시물을 작성](https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/)했다.
> 
> 더 안전한 메커니즘은 컨테이너 오케스트레이션 프레임워크에서 제공하는 비밀 지원을 사용하는 것이다. 대부분의 경우, 이러한 secret은 실행 중인 컨테이너에 파일로 마운트된다. MySQL 이미지와 todo 앱 등 많은 앱에서 변수가 포함된 파일을 가리키는 `_FILE` 접미사가 있는 env 변수를 지원하는 것을 볼 수 있다.
> 
> 예를 들어 `MYSQL_PASSWORD_FILE` 변수를 설정하면 앱에서 참조된 파일의 내용을 연결 비밀번호로 사용하게 된다. Docker는 이러한 환경 변수를 지원하기 위해 아무것도 하지 않는다. 앱이 변수를 찾고 파일 내용을 가져오는 방법을 알아야 한다.

이제 개발 준비 컨테이너를 시작할 수 있다.

<span>1.</span> 위의 각 환경 변수를 지정하고 컨테이너를 앱 네트워크에 연결한다. 이 명령을 실행할 때 `getting-started/app` 디렉터리에 있는지 확인하세요.

```bash
$ docker run -dp 127.0.0.1:3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:18-alpine \
   sh -c "yarn install && yarn run dev"
```

<span>2.</span> 컨테이너에 대한 로그(`docker logs -f <container-id>`)를 보면 다음과 유사한 메시지를 볼 수 있으며, 이는 컨테이너가 mysql 데이터베이스를 사용 중임을 나타낸다.

```
$ nodemon src/index.js
[nodemon] 2.0.20
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] starting `node src/index.js`
Connected to mysql db at host mysql
Listening on port 3000
```

<span>3.</span> 브라우저에서 앱을 열고 todo 목록에 몇 가지 항목을 추가한다.

<span>4.</span> mysql 데이터베이스에 연결하고 항목이 데이터베이스에 기록되고 있음을 보인다. 비밀번호는 `secret`이라는 점을 기억하세요.

```bash
$ docker exec -it <mysql-container-id> mysql -p todos
```

그리고 mysql 셸에서 다음을 실행한다.

```
mysql> select * from todo_items;
+--------------------------------------+--------------------+-----------+
| id                                   | name               | completed |
+--------------------------------------+--------------------+-----------+
| c906ff08-60e6-44e6-8f49-ed56a0853e85 | Do amazing things! |         0 |
| 2912a79e-8486-4bc3-a4c5-460793a575ab | Be awesome!        |         0 |
+--------------------------------------+--------------------+-----------+
```

## 다음 단계
이 시점에서 이제 별도의 컨테이너에서 실행되는 외부 데이터베이스에 데이터를 저장하는 애플리케이션이 생겼다. DNS를 사용한 컨테이너 네트워킹과 서비스 검색에 대해 조금 익혔다.

하지만 이 어플리케이션을 시작하기 위해 수행해야 하는 모든 작업이 다소 부담스러울 수 있다. 네트워크를 생성하고, 컨테이너를 시작하고, 모든 환경 변수를 지정하고, 포트를 노출하는 등의 작업을 수행해야 한다! 기억해야 할 것이 너무 많아서 다른 사람에게 전달하기가 더 어려울 것이다.

다음 Part에서는 Docker Compose에 대해 알아보겠다. Docker Compose를 사용하면 훨씬 더 쉬운 방법으로 어플리케이션 스택을 공유하고 다른 사람들이 하나의 간단한 명령으로 스택을 스핀업할 수 있다.
