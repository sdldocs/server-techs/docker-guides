Part 2에서는 todo 어플리케이션을 컨테이너화했다. 이 페이지에서는 어플리케이션과 컨테이너 이미지를 업데이트한다. 또한 컨테이너를 중지하고 제거하는 방법도 배울 것이다.

## 소스 코드 업데이트
아래 단계에서는 todo에 목록 항목이 없을 때의 "No item yet! Add one above!"를 "You have no todo items yet! Add one above!"로 변경하려고 한다. 

<span>1.</span> `src/static/js/app.js` 파일에서 56줄을 업데이트하여 새 빈 텍스트를 사용하세요.

```
- <p className="text-center">No item yet! Add one above!</p>
+ <p className="text-center">You have no todo items yet! Add one above!</p>
```

<span>2.</span> [Part 2](./containerize-application.md)에서 사용한 것과 동일한 `docker build` 명령을 사용하여 업데이트된 버전의 이미지를 빌드한다.

```bash
 docker build -t getting-started .
 ```

<span>3.</span> 업데이트된 코드를 사용하여 새 컨테이너를 시작한다.

```bash
$docker run -dp 127.0.0.1:3000:3000 getting-started
```

다음과 같은 오류가 표시될 것이다 (ID가 달라질 것입니다):

```
docker: Error response from daemon: driver failed programming external connectivity on endpoint kind_cohen (49943f37e799c4f909aa9559f79038824396727527abbbdb753ad6d4c4c5f097): Bind for 127.0.0.1:3000 failed: port is already allocated.
```

이전 컨테이너가 실행 중인 상태에서 새 컨테이너를 시작할 수 없기 때문에 오류가 발생했다. 이전 컨테이너가 이미 호스트의 포트 3000을 사용하고 있고 머신의 프로세스(컨테이너 포함) 중 하나만 특정 포트를 수신할 수 있기 때문dl다. 이 문제를 해결하려면 이전 컨테이너를 제거해야 한다.

## 기존 컨테이너 제거
컨테이너를 제거하려면 먼저 컨테이너를 중지시켜야 한다. 컨테이너가 중지되면 제거할 수 있다. CLI 또는 Docker Desktop의 그래픽 인터페이스를 사용하여 이전 컨테이너를 제거할 수 있다. 가장 편한 옵션을 선택하시오.

### CLI를 사용하여 컨테이너 제거
<span>1.<span> `docker ps` 명령을 사용하여 컨테이너의 ID를 가져온다.

```bash
$ docker ps
CONTAINER ID   IMAGE          COMMAND                   CREATED       STATUS       PORTS                      NAMES
a566213c8d07   1410826f58ca   "docker-entrypoint.s…"   6 hours ago   Up 6 hours   127.0.0.1:3000->3000/tcp   hungry_feistel
```

<span>1.<span> `docker stop` 명령을 사용하여 컨테이너를 중지시킨다. `<the-container-id>`를 `docker ps`의 ID로 바꾼다.

```bash
$ docker stop <the-container-id>
```

<span>3.</span> 컨테이너가 중지되면 `docker rm` 명령을 사용하여 컨테이너를 제거할 수 있다.

```bash
$ docker rm <the-container-id>
```

> **Note**:
> 
> `docker rm` 명령에 `force` 플래그를 추가하여 한 번의 명령으로 컨테이너를 중지하고 제거할 수 있다. 예를 들면, `docker rm -f <the-container-id>`이다.

## Start the updated app container 시작
<span>1.</span> 이제 `docker run` 명령을 사용하여 업데이트된 앱을 시작한다.

```bash
$ docker run -dp 127.0.0.1:3000:3000 getting-started
```

<span>2.</span> http://localhost:3000 에서 브라우저를 새로 고치면 업데이트된 도움말 텍스트를 볼 수 있다.

![](../images/get-started/screenshot_app_02.png)

## 다음 단계
업데이트를 빌드하는 동안 눈에 띄는 두 가지 사항이 있었다.

- todo 목록의 기존 항목이 모두 사라졌다! 좋은 앱이라 할 수 없다! 곧 수정할 예정이다.
- 이렇게 작은 변경을 위해 많은 단계를 거쳤다. 다음 파트에서는 변경할 때마다 다시 빌드하고 새 컨테이너를 시작할 필요 없이 코드 업데이트를 확인하는 방법에 대해 알아보겠다.

지속성에 대해 이야기하기 전에 이러한 이미지를 다른 사람들과 공유하는 방법을 살펴보겠다.


