이 가이드의 나머지 부분에서는 Node.js에서 실행되는 간단한 할일 목록 관리자를 예로 사용할 것이다. Node.js에 익숙하지 않더라도 걱정하지 마세요. 이 가이드는 JavaScript에 대한 사전 지식이 필요하지 않다.

이 가이드를 완료하려면 다음 사항이 필요하다.

- 로컬에서 실행 중인 Docker. 지침에 따라 [Docker를 다운로드하고 설치한다](../get-docker.md).
- [Git 클라이언트](https://git-scm.com/downloads)

> **Note**:
>
> Windows를 사용하며 Git Bash를 사용하여 Docker 명령을 실행하려는 경우 구문 차이에 대해서는 [Git Bash로 작업하기](https://docs.docker.com/desktop/troubleshoot/topics/#working-with-git-bash)를 참조하세요.

- 파일을 편집하기 위한 IDE 또는 텍스트 편집기. Docker는 [Visual Studio Code](https://code.visualstudio.com/) 사용을 권장한다.
- [컨테이너와 이미지](../overview.md#docker-objects)에 대한 개념적 이해.

## 앱 다운로드
어플리케이션을 실행하려면 먼저 어플리케이션 소스 코드를 컴퓨터에 가져와야 한다.

<span>1.</span> 다음 명령을 사용하여 [getting-started repository](https://github.com/docker/getting-started/tree/master)를 복제한다.

```bash
$ git clone https://github.com/docker/getting-started.git
```

<span>2.</span> 복제된 리포지토리의 내용을 확인한다. `getting-started/app` 디렉터리에는 `package.json` 파일과 두 개의 하위 디렉터리(`src`와 `spec`)가 있어야 한다.

```bash
$ tree -L 2
.
├── package.json
├── spec
│   ├── persistence
│   └── routes
├── src
│   ├── index.js
│   ├── persistence
│   ├── routes
│   └── static
└── yarn.lock

8 directories, 3 files
```

## 앱의 컨테이너 이미지 빌드
[컨테이너 이미지](../overview.md#docker-objects)를 빌드하려면 `Dockerfile`을 사용해야 한다. Dockerfile은 단순히 명령 스크립트가 포함된 파일 확장자가 없는 텍스트 기반 파일dl다. Docker는 이 스크립트를 사용하여 컨테이너 이미지를 빌드한다.

<span>1.</span> `package.json` 파일과 같은 위치인 `app` 디렉토리에 `Dockerfile`이라는 파일을 만든다. 아래 명령을 사용하여 운영 체제에 따라 Dockerfile을 생성할 수 있다. <br>
터미널에서 아래 나열된 다음 명령을 실행한다. <br>
현재 디렉터리를 `app` 디렉토리로 변경한다. `path/to/app`을 `getting-started/app` 디렉터리 경로로 바꾸고, `Dockerfile`이라는 빈 파일을 생성한다.<br>

```bash
$ cd getting-started/app
$ touch Dockerfile
```

<span>2.</span> 텍스트 편집기 또는 코드 편집기를 사용하여 다음 내용을 Dockerfile에 추가한다.

```dockerfile
# syntax=docker/dockerfile:1
   
FROM node:18-alpine
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

> **Note**:
>
> Dockerfile 예제에서 명령어를 선택하면 명령어에 대해 자세히 알아볼 수 있다.

<span>3.</span> 다음 명령을 사용하여 컨테이너 이미지를 빌드한다. <br>
터미널에서 현재 디렉터리를 `getting-started/app` 디렉토리로 변경한다. `path/to/app`을 `getting-started/app` 디렉터리 경로로 바구고, 컨테이너 이미지를 빌드한다. 

```bash
$ cd /path/to/app
$ docker build -t getting-started .
[+] Building 16.3s (13/13) FINISHED                                                                                      docker:desktop-linux
 => [internal] load build definition from Dockerfile                                                                                     0.0s
 => => transferring dockerfile: 185B                                                                                                     0.0s
 => [internal] load .dockerignore                                                                                                        0.0s
 => => transferring context: 2B                                                                                                          0.0s
 => resolve image config for docker.io/docker/dockerfile:1                                                                               2.9s
 => [auth] docker/dockerfile:pull token for registry-1.docker.io                                                                         0.0s
 => docker-image://docker.io/docker/dockerfile:1@sha256:39b85bbfa7536a5feceb7372a0817649ecb2724562a38360f4d6a7782a409b14                 1.0s
 => => resolve docker.io/docker/dockerfile:1@sha256:39b85bbfa7536a5feceb7372a0817649ecb2724562a38360f4d6a7782a409b14                     0.0s
 => => sha256:39b85bbfa7536a5feceb7372a0817649ecb2724562a38360f4d6a7782a409b14 8.40kB / 8.40kB                                           0.0s
 => => sha256:7f44e51970d0422c2cbff3b20b6b5ef861f6244c396a06e1a96f7aa4fa83a4e6 482B / 482B                                               0.0s
 => => sha256:a28edb2041b8f23c38382d8be273f0239f51ff1f510f98bccc8d0e7f42249e97 2.90kB / 2.90kB                                           0.0s
 => => sha256:9d0cd65540a143ce38aa0be7c5e9efeed30d3580d03667f107cd76354f2bee65 10.82MB / 10.82MB                                         0.9s
 => => extracting sha256:9d0cd65540a143ce38aa0be7c5e9efeed30d3580d03667f107cd76354f2bee65                                                0.1s
 => [internal] load metadata for docker.io/library/node:18-alpine                                                                        2.2s
 => [auth] library/node:pull token for registry-1.docker.io                                                                              0.0s
 => [1/4] FROM docker.io/library/node:18-alpine@sha256:93d91deea65c9a0475507e8bc8b1917d6278522322f00c00b3ab09cab6830060                  3.2s
 => => resolve docker.io/library/node:18-alpine@sha256:93d91deea65c9a0475507e8bc8b1917d6278522322f00c00b3ab09cab6830060                  0.0s
 => => sha256:8c6d1654570f041603f4cef49c320c8f6f3e401324913009d92a19132cbf1ac0 3.33MB / 3.33MB                                           0.4s
 => => sha256:82f2fd2295bacb1d1d87648cdb335959765d10261f3073a9cdc5c1bfc73b755d 47.49MB / 47.49MB                                         1.7s
 => => sha256:db6d709ab241a42ba2da21349394161689529fdbddbf781e3c71f2f7f71ba492 2.34MB / 2.34MB                                           0.7s
 => => sha256:93d91deea65c9a0475507e8bc8b1917d6278522322f00c00b3ab09cab6830060 1.43kB / 1.43kB                                           0.0s
 => => sha256:c9af644d2cbe040513f583f406acd066a26bda28f770feb71d35744781ade432 1.16kB / 1.16kB                                           0.0s
 => => sha256:bfeb0d67cd70f5b7cb834229f31b84451eb03f945ceed0bf179f57b7ac1bd63b 6.75kB / 6.75kB                                           0.0s
 => => extracting sha256:8c6d1654570f041603f4cef49c320c8f6f3e401324913009d92a19132cbf1ac0                                                0.1s
 => => sha256:0d7ff98fc48ae2f8d260c99c48e45757a17bc1b9486362db0e0181fc8cda521a 448B / 448B                                               0.7s
 => => extracting sha256:82f2fd2295bacb1d1d87648cdb335959765d10261f3073a9cdc5c1bfc73b755d                                                1.4s
 => => extracting sha256:db6d709ab241a42ba2da21349394161689529fdbddbf781e3c71f2f7f71ba492                                                0.0s
 => => extracting sha256:0d7ff98fc48ae2f8d260c99c48e45757a17bc1b9486362db0e0181fc8cda521a                                                0.0s
 => [internal] load build context                                                                                                        0.0s
 => => transferring context: 4.59MB                                                                                                      0.0s
 => [2/4] WORKDIR /app                                                                                                                   0.1s
 => [3/4] COPY . .                                                                                                                       0.0s
 => [4/4] RUN yarn install --production                                                                                                  6.3s
 => exporting to image                                                                                                                   0.5s 
 => => exporting layers                                                                                                                  0.5s 
 => => writing image sha256:1410826f58ca6fdaac5c9d056629da122e87fa3829a4058a832939da5e8b6d4c                                             0.0s 
 => => naming to docker.io/library/getting-started                                                                                       0.0s 
                                                                                                                                              
What's Next?
  View summary of image vulnerabilities and recommendations → docker scout quickview
```

`docker build` 명령은 Dockerfile을 사용하여 새 컨테이너 이미지를 빌드한다. Docker가 많은 "레이어"를 다운로드한 것을 보았을 것이다. 이는 빌더에 `node:18-alpine` 이미지에서 시작하도록 지시했기 때문이다. 하지만 머신에 해당 이미지가 없었기 때문에 Docker가 이미지를 다운로드해야 했다.

Docker가 이미지를 다운로드한 후 Dockerfile의 명령을 어플리케이션에 복사하고 `yarn`을 사용하여 어플리케이션의 종속성을 설치했다. `CMD` 명령어는 이 이미지에서 컨테이너를 시작할 때 실행할 기본 명령을 지정한다.

마지막으로 `-t` 플래그는 이미지에 태그를 지정한다. 이것은 단순히 최종 이미지의 사람이 읽을 수 있는 이름이라고 생각하면 된다. 이미지의 이름을 `getting-started` 이미지로 지정했으므로 컨테이너를 실행할 때 해당 이미지를 참조할 수 있다.

docker 빌드 명령의 끝에 있는 `.`은 현재 디렉터리에서 Dockerfile을 찾으라고 Docker에 지시하는 것이다.

## 앱 컨테이너 시작
이제 이미지가 생겼으니 컨테이너에서 어플리케이션을 실행할 수 있다. 이를 위해 `docker run` 명령을 사용한다.

<span>1.</span> `docker run` 명령을 사용하여 컨테이너를 시작하고 방금 만든 이미지의 이름을 지정한다.

```bash
$ docker run -dp 127.0.0.1:3000:3000 getting-started
a566213c8d0727392936a83ccc452cecca601ed92f66306ede36638eb01b9812
```

`-d` 플래그(`--detach`의 줄임말)는 컨테이너를 백그라운드에서 실행한다. `-p` 플래그(`--publish`의 줄임말)는 호스트와 컨테이너 간에 포트 매핑을 생성한다. `-p` 플래그는 `HOST:CONTAINER` 형식의 문자열 값을 사용하며, 여기서 `HOST`는 호스트의 주소이고 `CONTAINER`는 컨테이너의 포트이다. 여기에 표시된 명령은 컨테이너의 포트 3000을 호스트의 `127.0.0.1:3000`(`localhost:3000`)에 게시한다. 포트 매핑이 없으면 호스트에서 어플리케이션에 액세스할 수 없다.

<span>2.</span> 몇 초 후 웹 브라우저를 열어 `http://localhost:3000` 으로 이동한다. 앱을 볼 수 있어야 한다.

![](../images/get-started/screenshot_app_01.png)

<span>3.</span> 계속해서 한두 개의 항목을 추가하고 예상대로 작동하는지 확인한다. 항목을 완료로 표시하고 제거할 수 있다. 프론트엔드에서 백엔드에 항목을 성공적으로 저장하고 있다.

터미널에서 다음 `docker ps` 명령을 실행하여 컨테이너 목록을 출력한다.

```bash
$ docker ps
CONTAINER ID   IMAGE             COMMAND                   CREATED         STATUS         PORTS                      NAMES
a566213c8d07   getting-started   "docker-entrypoint.s…"   5 minutes ago   Up 5 minutes   127.0.0.1:3000->3000/tcp   hungry_feistel
```

## 다음 단계
이 짧은 페이지에서는 컨테이너 이미지를 빌드하기 위한 Dockerfile 생성에 대한 기본 사항을 배웠다. 이미지를 빌드한 후 컨테이너를 시작하고 실행 중인 앱을 확인했다.

다음으로, 앱을 수정하고 실행 중인 어플리케이션을 새 이미지로 업데이트하는 방법을 배워보겠다. 그 과정에서 몇 가지 다른 유용한 명령어를 배우게 될 것이다.
