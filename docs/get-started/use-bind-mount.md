[Part 5](persist-db.md)에서는 볼륨 마운트를 사용하여 데이터베이스의 데이터를 유지했다. 볼륨 마운트는 어플리케이션 데이터를 영구적으로 저장할 곳이 필요할 때 좋은 방법이다.

바인드 마운트(bind mount)는 마운트의 다른 유형으로, 호스트의 파일 시스템에서 컨테이너로 디렉터리를 공유할 수 있다. 어플리케이션에서 작업할 때 바인드 마운트를 사용하여 소스 코드를 컨테이너에 마운트할 수 있다. 컨테이너는 파일을 저장하자마자 코드에 대한 변경 사항을 즉시 확인한다. 즉, 컨테이너에서 파일 시스템 변경을 감시하고 이에 응답하는 프로세스를 실행할 수 있다.

이 Part에서는 바인드 마운트와 [nodemon](https://npmjs.com/package/nodemon)이라는 도구를 사용하여 파일 변경을 감시하고 어플리케이션을 자동으로 다시 시작하는 방법을 살펴본다. 대부분의 다른 언어와 프레임워크에도 이와 유사한 도구가 있다.

## 볼륨 타입 빠른 비교
다음 표에 볼륨 마운트와 바인드 마운트의 주요 차이점을 요약하였다.

|   | **Named volumes** | **Bind mounts** |
|---|-------------------|-----------------|
| Host Loaction | Docker 선택 | 사용자 선택 |
| Mount 예 (using `--mount`) | `type=volume,src=my-volume,target=/usr/local/data` | `type=bind,src=/path/to/data,target=/usr/local/data` |
| 컨테이너 내용물로 새 볼륨을 채운다 | yes | no |
| 볼륨 드라이버 지원 | yes | no |

## 바인드 마운트 사용해 보기
어플리케이션 개발에 바인드 마운트를 사용하는 방법을 살펴보기 전에 간단한 실험을 실행하여 바인드 마운트의 작동 방식을 실제로 이해할 수 있다.

<span>1.</span> 터미널을 열고 현재 디렉터리를 `gettng-started` 리포지토리의 `app` 디렉토리로 이동한다.

```bash
$ docker run -it --mount type=bind,src="$(pwd)",target=/src ubuntu bash
```

`--mount` 옵션은 호스트 머신의 현재 작업 디렉터리(`getting-started/app`)인 `src`와 컨테이너 내부에 해당 디렉터리가 표시되어야 하는 위치(`/src)`인 `target`을 지정하여 바인드 마운트를 생성하도록 Docker에 지시한다.

<span>3.</span> 명령을 실행하면 Docker는 컨테이너의 파일 시스템 루트 디렉터리에서 대화형 `bash` 세션을 시작한다.

```bash
root@ac1237fad8db:/# pwd
/
root@ac1237fad8db:/# ls
bin   dev  home  media  opt   root  sbin  srv  tmp  var
boot  etc  lib   mnt    proc  run   src   sys  usr
```

<span>4.</span> 디렉터리를 `src` 디렉터리로 변경한다.

이 디렉터리는 컨테이너를 시작할 때 마운트한 디렉터리이다. 이 디렉터리의 내용을 나열하면 호스트 머신의 `getting-started/app` 디렉터리에 있는 것과 동일한 파일이 디스플레이된다.

```bash
root@ac1237fad8db:/# cd src
root@ac1237fad8db:/src# ls
Dockerfile  node_modules  package.json  spec  src  yarn.lock
```

<span>5.</span> `myfile.txt`라는 이름의 새 파일을 만든다.

```bash
root@ac1237fad8db:/src# touch myfile.txt
root@ac1237fad8db:/src# ls
Dockerfile  myfile.txt  node_modules  package.json  spec  src  yarn.lock
```

<span>6.</span> 호스트에서 `app` 디렉터리를 열고 디렉터리에 `myfile.txt` 파일이 있는지 확인한다.

```
├── app/
│ ├── Dockerfile
│ ├── myfile.txt
│ ├── node_modules/
│ ├── package.json
│ ├── spec/
│ ├── src/
│ └── yarn.lock
```

<span>7.</span> 호스트에서 `myfile.txt` 파일을 삭제한다.

<span>8.</span> 컨테이너에서 `app` 디렉터리의 내용을 다시 한 번 나열한다. 이제 파일이 삭제되었는지 확인한다.

```
root@ac1237fad8db:/src# ls
Dockerfile  node_modules  package.json  spec  src  yarn.lock
```

<span>9.</span> `Ctrl + D`로 대화형 컨테이너 세션을 중지한다.

여기까지 바인드 마운트에 대한 간략한 소개가 끝났다. 이 절차에서는 호스트와 컨테이너 간에 파일이 공유되는 방법과 변경 사항이 양쪽 모두에 즉시 반영되는 방법을 설명했다. 이제 바인드 마운트를 사용하여 소프트웨어를 개발할 수 있다.

## 컨테이너 개발
일반적으로 로컬 개발 설정에 바인드 마운트를 사용한다. 개발 머신에 모든 빌드 도구와 환경이 설치되어 있지 않아도 된다는 장점이 있다. 단 한 번의 docker run 명령으로 종속 요소와 도구를 가져올 수 있다.

### 개발 컨테이너에서 앱 실행
다음 단계에서는 아래 사항을 수행하는 바인드 마운트를 사용하여 개발 컨테이너를 실행하는 방법을 설명한다.

- 소스 코드를 컨테이너에 마운트
- 모든 종속성 설치
- `nodemon`을 시작하여 파일 시스템 변경을 감시한다.

CLI 또는 Docker Desktop을 사용하여 바인드 마운트로 컨테이너를 실행할 수 있다.

<span>1.</span> 현재 실행 중인 `getting-started` 컨테이너가 없는지 확인한다.

<span>2.</span> `getting-started/app` 디렉토리에서 다음 명령을 실행한다.

```bash
$ docker run -dp 127.0.0.1:3000:3000 \
    -w /app --mount type=bind,src="$(pwd)",target=/app \
    node:18-alpine \
    sh -c "yarn install && yarn run dev"
```

다음은 명령에 대한 설명이다.

- `-dp 127.0.0.1:3000:3000`: 이전과 동일하다. 분리(백그라운드) 모드에서 실행하고 포트 매핑을 생성한다.
- `-w /app`: 명령이 실행될 "작업 디렉토리" 또는 현재 디렉터리를 설정한다.
- `--mount type=bind,src="$(pwd)",target=/app`: 호스트의 현재 디렉터리를 컨테이너의 `/app` 디렉터리로 바인딩 마운트한다.
- `node:18-alpine`: 사용할 이미지. 이 이미지는 Dockerfile의 앱에 대한 기본 이미지이다.
- `sh -c "yarn install && yarn run dev"`: `sh`를 사용하여 셸을 시작하고(알파인에는 bash가 없음), `yarn install`을 실행하여 패키지를 설치한 다음, `yarn run dev`를 실행하여 개발 서버를 시작한다. `package.json`을 보면 개발 스크립트가 `nodemon`을 시작하는 것을 볼 수 있다.

<span>3.</span> `docker logs <container-id>`를 사용하여 로그를 볼 수 있다. 이 로그가 표시되면 준비가 완료된 것이다.

```bash
$ docker logs -f <container-id>
nodemon src/index.js
[nodemon] 2.0.20
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] starting `node src/index.js`
Using sqlite database at /etc/todos/todo.db
Listening on port 3000
```

로그 보기를 마치면 `Ctrl+C`를 눌러 종료한다.

### 개발 컨테이너에서 앱 개발
호스트 머신에서 앱을 업데이트하고 컨테이너에 반영된 변경 사항을 확인한다.

<span>1.</span> `src/static/js/app.js` 파일의 109줄에 있는 '항목 추가' 버튼을 단순히 '추가'로 변경한다.

```
- {submitting ? 'Adding...' : 'Add Item'}
+ {submitting ? 'Adding...' : 'Add'}
```

파일을 저장한다.

<span>2.</span> 웹 브라우저에서 페이지를 새로 고침하면 변경 사항이 거의 즉시 반영되는 것을 확인할 수 있다. Node 서버가 다시 시작되는 데 몇 초 정도 걸릴 수 있다. 오류가 발생하면 몇 초 후에 새로 고침을 시도하세요.

![](../images/get-started/updated-add-button.png)

<span>3.</span> 그 외 변경하고 싶은 사항이 있으면 자유롭게 변경하세요. 변경하고 파일을 저장할 때마다 `nodemon` 프로세스가 컨테이너 내부의 앱을 자동으로 재시작한다. 완료되면 컨테이너를 중지하고 다음을 사용하여 새 이미지를 빌드한다.

```bash
$ docker build -t getting-started .
```

## 다음 단계
이제 이미지를 다시 빌드하지 않고도 데이터베이스를 유지하고 개발하면서 앱의 변경 사항을 확인할 수 있다.

볼륨 마운트와 바인드 마운트 외에도 Docker는 보다 복잡하고 특수한 사용 사례를 처리하기 위한 다른 마운트 타입과 스토리지 드라이버도 지원한다. 고급 스토리지 개념에 대해 자세히 알아보려면 [Managing data in Docker](https://docs.docker.com/storage/)를 참조하세요.

앱을 프로덕션에 맞게 준비하려면 SQLite에서 작동하는 데이터베이스를 조금 더 확장하기 쉬운 것으로 마이그레이션해야 한다. 간단하게 하기 위해 관계형 데이터베이스를 계속 사용하고 어플리케이션에서 MySQL을 사용하도록 전환하겠다. 하지만 MySQL을 어떻게 실행해야 할까? 컨테이너가 서로 대화할 수 있도록 하려면 어떻게 해야 할까? 다음 Part에서 이에 대해 알아보겠다.
