이제 이미지를 빌드했으니 공유할 수 있다. Docker 이미지를 공유하려면 Docker 레지스트리를 사용해야 한다. 기본 레지스트리는 Docker Hub이며 사용한 모든 이미지의 출처가 여기에 있다.

> **Docker ID**
>
> Docker ID를 사용하면 컨테이너 이미지를 위한 세계 최대 규모의 라이브러리이자 커뮤니티인 Docker Hub에 액세스할 수 있다. [Docker ID](https://hub.docker.com/signup?_gl=1*ujpkqs*_ga*MjA1ODI4ODk5NS4xNjg5NzQxNjA2*_ga_XJWPQMJYHQ*MTY5MDY5NzgxNC4xNC4xLjE2OTA2OTc4MTYuNTguMC4w)가 없는 경우 무료로 생성할 수 있다.

### 리포지토리 생성
이미지를 푸시하려면 먼저 Docker Hub에 리포지토리를 만들어야 한다.

<span>1.</span> [Docker Hub](https://hub.docker.com/?_gl=1*18qsgnk*_ga*MjA1ODI4ODk5NS4xNjg5NzQxNjA2*_ga_XJWPQMJYHQ*MTY5MDY5NzgxNC4xNC4xLjE2OTA2OTkwMjMuNjAuMC4w)에 `Sign up`하거나 `Sign in`한다.

<span>2.</span> **Create Repository** 버튼을 선택한다.

<span>3.</span> 리포지토리 이름으로 `fetting-started`를 사용한다. 공개 여부가 `Public`인지 확인한다.

> **Private 리포지토리**
>
> 특정 사용자 또는 팀으로 콘텐츠를 제한할 수 있는 비공개 리포지토리를 제공한다. 자세한 내용은 [Docker pricing](https://www.docker.com/pricing?utm_source=docker&utm_medium=webreferral&utm_campaign=docs_driven_upgrade&_gl=1*i8jq9j*_ga*MjA1ODI4ODk5NS4xNjg5NzQxNjA2*_ga_XJWPQMJYHQ*MTY5MDY5NzgxNC4xNC4xLjE2OTA2OTc4MTYuNTguMC4w) 페이지에서 확인하세요.

<span>4.<span> **Create**를 선택한다.

아래 이미지를 보면 Docker 명령의 예를 볼 수 있다. 이 명령으로 이 리포지토리에 푸시할 수 있다.

![](../images/get-started/push-command.png)

## 이미지 푸시
<span>1.</span> 명령어에 Docker Hub에 표시되는 푸시 명령을 실행해 보세요. 명령은 "docker"가 아닌 네임스페이스를 사용한다는 점에 유의하세요.

```bash
docker push docker/getting-started
Using default tag: latest
The push refers to repository [docker.io/docker/getting-started]
An image does not exist locally with the tag: docker/getting-started
```

왜 실패했을까? 푸시 명령이 docker/getting-started라는 이름의 이미지를 찾고 있었지만 찾지 못했다. `docker image ls`를 실행해도 이미지가 표시되지 않는다.

이 문제를 해결하려면 빌드한 기존 이미지에 '태그'를 지정하여 다른 이름을 지정해야 한다.

<span>2.</span> `docker login -u YOUR-USER-NAME` 명령을 사용하여 Docker Hub에 로그인한다.

<span>3.</soan> `docker tag` 명령을 사용하여 `getting-started` 이미지에 새 이름을 지정한다. `YOUR-USER-NAME`을 Docker ID로 바꿔야 한다.

```bash
$ docker tag getting-started YOUR-USER-NAME/getting-started
```

`docker tag` 명령에 대해 자세히 알아보려면 [docker tag](https://docs.docker.com/engine/reference/commandline/tag/)를 참조하세요.

<span>4.</span> 이제 푸시 명령을 다시 시도해 보자. Docker Hub에서 값을 복사하는 경우 이미지 이름에 태그를 추가하지 않았으므로 `tagname` 부분을 삭제할 수 있다. 태그를 지정하지 않으면 Docker는 `latest`이라는 태그를 사용한다.

```bash
$ docker push YOUR-USER-NAME/getting-started
```

## 새 인스턴스에서 이미지 실행
이제 이미지가 빌드되어 레지스트리에 푸시되었으므로 이 컨테이너 이미지를 본 적이 없는 새로운 인스턴스에서 앱을 실행해 보세요. 이를 위해 Play with Docker를 사용한다.

> **Note**:
>
> Play with Docker는 amd64 플랫폼을 사용합니다. Apple Silicon이 탑재된 ARM 기반 Mac을 사용하는 경우, 이미지를 다시 빌드하여 Play with Docker와 호환되도록 하고 새 이미지를 리포지토리에 푸시해야 합니다.
> 
> amd64 플랫폼용 이미지를 빌드하려면 `--platform` 플래그를 사용한다.
>
> ```bash
> $ docker build --platform linux/amd64 -t YOUR-USER-NAME/> getting-started .
>```
>
> Docker buildx는 멀티플랫폼 이미지 빌드도 지원한다. 자세한 내용은 [multi-platform images](https://docs.docker.com/build/building/multi-platform/)를 참조하세요.

<span>1.<span> 브라우저를 열어 [Play with Docker](https://labs.play-with-docker.com/?_gl=1*j28fwk*_ga*MjA1ODI4ODk5NS4xNjg5NzQxNjA2*_ga_XJWPQMJYHQ*MTY5MDc1NzA1OS4xNi4xLjE2OTA3NTcyMDUuNTkuMC4w)로 이동한다.

<span>2.<span> **Login**을 선택한 다음 드롭다운 목록에서 `docker`를 선택한다.

<span>3.<span> Docker Hub 계정에 연결한다.

<span>4.<span> 로그인한 다음, 왼쪽 바에서 **ADD NEW INSTANCE** 옵션을 선택한다. 옵션이 보이지 않으면 브라우저를 조금 더 넓게 학장하여 본다. 몇 초 후 브라우저에 터미널 창이 열린다.

![](../images/get-started/pwd-add-new-instance.png)

<span>5.<span> 터미널에서 새로 푸시한 앱을 시작한다.

```bash
$ docker run -dp 0.0.0.0:3000:3000 YOUR-USER-NAME/getting-started
```

이미지가 아래로 내려가고 결국 시작되는 것을 볼 수 있을 것이다.

> **Tip**
>
> 이 명령이 포트를 다른 IP 주소로 매핑하여 바인딩하는 것을 볼 수 있다. 이전 `docker run` 명령은 포트를 호스트의 `127.0.0.1:3000`에 게시했다. 이번에는 `0.0.0.0`을 사용한다.
> 
> `127.0.0.1`에 바인딩하면 오직 컨테이너의 포트만 루프백 인터페이스에 노출된다. 그러나 `0.0.0.0`에 바인딩하면 호스트의 모든 인터페이스에 컨테이너의 포트가 노출되어 외부에서 사용할 수 있다.
> 
> 포트 매핑 작동 방식에 대한 자세한 내용은 [Networking](https://docs.docker.com/network/#published-ports)을 참조하세요.

<span>6.<span> 3000 배지가 나타나 이를 선택하면 수정한 앱이 표시된다. 3000 배지가 표시되지 않으면 **Open Port** 버튼을 선택하고 3000을 입력하면 된다.

## 다음 단계
이 Part에서는 이미지를 레지스트리에 푸시하여 공유하는 방법을 배웠다. 그런 다음 새 인스턴스로 이동하여 새로 푸시된 이미지를 실행할 수 있었다. 파이프라인이 이미지를 만들어 레지스트리에 푸시하면 프로덕션 환경에서 최신 버전의 이미지를 사용할 수 있는 CI 파이프라인에서는 매우 일반적이다.

이제 지난 Part의 마지막 부분에서 살펴본 내용을 다시 살펴보겠다. 앱을 다시 시작하면 todo 목록 항목이 모두 사라진다는 것을 알았다. 이는 분명 좋은 사용자 경험이 아니므로 다음 Part에서는 재시작 시 데이터를 유지하는 방법에 대해 알아보겠다.
