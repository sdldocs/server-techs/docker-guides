## 전제 조건
[개발에 컨테이너 사용](https://docs.docker.com/language/nodejs/develop/)에서 이미지를 빌드하고 컨테이너화된 애플리케이션으로 실행하는 단계를 살펴보세요.

## 소개
테스트는 최신 소프트웨어 개발에서 필수적인 부분이다. 테스트는 개발 팀에 따라 다양한 의미를 가질 수 있다. 단위 테스트, 통합 테스트, 엔드투엔드 테스트가 있다. 이 가이드에서는 Docker에서 단위 테스트를 실행하는 방법을 살펴본다.

## 테스트 만들기
어플리케이션 내의 `./test` 디렉터리에 Mocha 테스트를 정의해 보겠다.

```bash
$ mkdir -p test
```

`./test/test.js`에 다음 코드를 저장한다.

```js
var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});
```

### 로컬에서 실행과 어플리케이션 
Docker 이미지를 빌드하고 모든 것이 제대로 실행되는지 확인해 보겠다. 다음 명령을 실행하여 컨테이너에서 Docker 이미지를 빌드하고 실행한다.

```bash
$ docker compose -f docker-compose.dev.yml up --build
```

이제 JSON 페이로드를 POST하여 어플리케이션을 테스트하고, HTTP GET 요청을 수행하여 JSON이 정확히 저장되었는지 확인해 보겠다.

```bash
curl --request POST \
  --url http://localhost:8000/test \
  --header 'content-type: application/json' \
  --data '{"msg": "testing"}'
```

이제 동일한 엔드포인트에 GET 요청을 수행하여 JSON 페이로드가 올바르게 저장되고 검색되었는지 확인한다. "id"와 "createDate"가 달라질 것이다.

```bash
curl http://localhost:8000/test

{"code":"success","payload":[{"msg":"testing","id":"e88acedb-203d-4a7d-8269-1df6c1377512","createDate":"2020-10-11T23:21:16.378Z"}]}
```

## Mocha 설치
다음 명령을 실행하여 Mocha를 설치하고 개발자 종속성에 추가한다.

```bash
npm install --save-dev mocha
```

## 테스트 실행을 위한 `package.jason`과 Dockerfile 업데이트
이제 어플리케이션이 제대로 실행되고 있다는 것을 알았으니 컨테이너 내부에서 테스트를 실행해 보겠다. 위에서 사용한 것과 동일한 docker run 명령을 사용하겠지만 이번에는 컨테이너 내부에 있는 CMD를 npm run test로 재정의하겠다. 이렇게 하면 package.json 파일의 "script" 섹션 아래에 있는 명령이 호출된다. 아래를 참조하세요.

```
{
...
  "scripts": {
    "test": "mocha ./**/*.js",
    "start": "nodemon --inspect=0.0.0.0:9229 -L server.js"
  },
...
}
```

다음은 컨테이너를 시작하고 테스트를 실행하는 Docker 명령이다.

```bash
$ docker compose -f docker-compose.dev.yml run notes npm run test
```

테스트를 실행하면 다음과 같은 오류가 표시된다.

```
> mocha ./**/*.js

sh: mocha: not found
```

현재 Dockerfile은 이미지에 개발 종속성을 설치하지 않으므로 모카를 찾을 수 없다. 이 문제를 해결하려면 Dockerfile을 업데이트하여 개발 종속성을 설치할 수 있다.

```dockerfile
# syntax=docker/dockerfile:1

FROM node:latest
ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --include=dev

COPY . .

CMD ["node", "server.js"]
```

명령을 다시 실행하고 이번에는 새 Dockerfile을 사용하도록 이미지를 다시 빌드한다.

```bash
$ docker compose -f docker-compose.dev.yml run --build notes npm run test
```

이번에 테스트를 실행하면 다음과 같은 출력이 디스플레이된다.

```
> mocha ./**/*.js

  Array
    #indexOf()
      ✔ should return -1 when the value is not present

  1 passing (6ms)
```

개발 종속성이 설치된 이 이미지는 프로덕션 이미지에 적합하지 않다. 여러 Dockerfile을 만드는 대신 다단계 Dockerfile을 생성하여 테스트용 이미지와 프로덕션용 이미지를 만들 수 있다.

### 테스팅을 위한 다단계 Dockerfile
명령에 따라 테스트를 실행하는 것 외에도, 다단계 Dockerfile을 사용하여 이미지를 빌드할 때 테스트를 실행할 수 있다. 다음 Dockerfile은 테스트를 실행하고 프로덕션 이미지를 빌드한다.

```dockerfile
# syntax=docker/dockerfile:1
FROM node:latest as base

WORKDIR /code

COPY package.json package.json
COPY package-lock.json package-lock.json

FROM base as test
RUN npm ci
COPY . .
CMD ["npm", "run", "test"]

FROM base as prod
RUN npm ci --production
COPY . .
CMD ["node", "server.js"]
```

먼저 FROM `node:latest` 문에 레이블 `as base`를 추가한다. 이렇게 하면 다른 빌드 단계에서 이 빌드 단계를 참조할 수 있다. 다음으로 test라는 레이블이 붙은 새 빌드 단계를 추가한다. 테스트를 실행하는 데 이 단계를 사용할 것이다.

이제 이미지를 다시 빌드하고 테스트를 실행해 보겠다. 위와 동일한 docker build 명령을 실행하지만 이번에는 테스트 빌드 단계를 특별히 실행하도록 `--target test` 플래그를 추가한다.

```bash
docker build -t node-docker --target test .
[+] Building 66.5s (12/12) FINISHED
 => [internal] load build definition from Dockerfile                                                                                                                 0.0s
 => => transferring dockerfile: 662B                                                                                                                                 0.0s
 => [internal] load .dockerignore
 ...
  => [internal] load build context                                                                                                                                    4.2s
 => => transferring context: 9.00MB                                                                                                                                  4.1s
 => [base 2/4] WORKDIR /code                                                                                                                                         0.2s
 => [base 3/4] COPY package.json package.json                                                                                                                        0.0s
 => [base 4/4] COPY package-lock.json package-lock.json                                                                                                              0.0s
 => [test 1/2] RUN npm ci                                                                                                                                            6.5s
 => [test 2/2] COPY . .
 ```

 이제 테스트 이미지가 빌드되었으므로 컨테이너에서 실행하여 테스트가 통과하는지 확인할 수 있다.

 ```bash
$ docker run -it --rm -p 8000:8000 node-docker

> node-docker@1.0.0 test /code
> mocha ./**/*.js



  Array
    #indexOf()
      ✓ should return -1 when the value is not present


  1 passing (12ms)
```

빌드 출력을 잘라냈지만 Mocha 테스트 러너가 완료되고 모든 테스트가 통과된 것을 볼 수 있다.

이것은 훌륭하지만 현재로서는 테스트를 빌드하고 실행하기 위해 두 개의 도커 명령을 실행해야 한다. 테스트 단계에서 CMD 문 대신 RUN 문을 사용하면 이 문제를 약간 개선할 수 있다. CMD 문은 이미지를 빌드하는 동안 실행되지 않고 컨테이너에서 이미지를 실행할 때 실행된다. RUN 문을 사용하면 이미지 빌드 중에 테스트가 실행되고 실패하면 빌드가 중지된다.

아래와 같이 Dockerfile을 업데이트한다.

```dockerfile
# syntax=docker/dockerfile:1
FROM node:18-alpine as base

WORKDIR /code

COPY package.json package.json
COPY package-lock.json package-lock.json

FROM base as test
RUN npm ci
COPY . .
RUN npm run test

FROM base as prod
RUN npm ci --production
COPY . .
CMD ["node", "server.js"]
```

이제 테스트를 실행하려면 위와 같이 docker build 명령을 실행하기만 하면 된다.

```bash
$ docker build -t node-docker --target test .
[+] Building 8.9s (13/13) FINISHED
 => [internal] load build definition from Dockerfile      0.0s
 => => transferring dockerfile: 650B                      0.0s
 => [internal] load .dockerignore                         0.0s
 => => transferring context: 2B

> node-docker@1.0.0 test /code
> mocha ./**/*.js



  Array
    #indexOf()
      ✓ should return -1 when the value is not present


  1 passing (9ms)

Removing intermediate container beadc36b293a
 ---> 445b80e59acd
Successfully built 445b80e59acd
Successfully tagged node-docker:latest
```

간단하게 하기 위해 출력을 다시 잘랐지만 테스트가 실행되고 통과된 것을 볼 수 있다. 테스트 중 하나를 중단하고 테스트가 실패했을 때의 출력을 관찰해 보겠다.

`test/test.js` 파일을 열고 5번째 줄을 다음과 같이 변경한다.

```js
1	var assert = require('assert');
2	describe('Array', function() {
3	  describe('#indexOf()', function() {
4	    it('should return -1 when the value is not present', function() {
5	      assert.equal([1, 2, 3].indexOf(3), -1);
6	    });
7	  });
8	});
```

이제 위에서 동일한 docker build 명령을 실행하고 빌드가 실패하고 실패한 테스트 정보가 콘솔에 인쇄되는지 확인한다.

```bash
docker build -t node-docker --target test .
Sending build context to Docker daemon  22.35MB
Step 1/8 : FROM node:18-alpine as base
 ---> 995ff80c793e
...
Step 8/8 : RUN npm run test
 ---> Running in b96d114a336b

> node-docker@1.0.0 test /code
> mocha ./**/*.js



  Array
    #indexOf()
      1) should return -1 when the value is not present


  0 passing (12ms)
  1 failing

  1) Array
       #indexOf()
         should return -1 when the value is not present:

      AssertionError [ERR_ASSERTION]: 2 == -1
      + expected - actual

      -2
      +-1
      
      at Context.<anonymous> (test/test.js:5:14)
      at processImmediate (internal/timers.js:461:21)



npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! node-docker@1.0.0 test: `mocha ./**/*.js`
npm ERR! Exit status 1
...
```

## 다음 단계
이 모듈에서는 Docker 이미지 빌드 프로세스의 일부로 테스트를 실행하는 방법을 살펴보았다.

다음 모듈에서는 GitHub Actions를 사용하여 CI/CD 파이프라인을 설정하는 방법을 살펴보겠다.
