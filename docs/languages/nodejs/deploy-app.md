이제 CI/CD 파이프라인을 구성했으니 어플리케이션을 배포하는 방법을 살펴보자. Docker는 Azure ACI와 AWS ECS에 컨테이너 배포를 지원한다. Docker Desktop에서 Kubernetes를 활성화한 경우 어플리케이션을 Kubernetes에 배포할 수도 있다. (일단 마지막인 Docker Desktop에서 Kubernetes를 활성화하는 경무만 편역한다.)

## Kubernetes
Docker Desktop에는 standalone Kubernetes 서버와 클라이언트는 물론, 머신에서 실행되는 Docker CLI 통합이 포함되어 있다. Kubernetes를 활성화하면 Kubernetes에서 워크로드를 테스트할 수 있다.

Kubernetes를 활성화하려면 다음과 같이 한다.

1. Docker 메뉴에서 설정을 선택한다.
2. Kubernetes를 선택하고 Kubernetes 활성화를 클릭한다.

    그러면 Docker 데스크톱이 시작될 때 Kubernetes 단일 노드 클러스터가 시작된다.

자세한 내용은 [Kubernetes에 배포하기](https://docs.docker.com/desktop/kubernetes/)와 [Kubernetes YAML을 사용하여 앱 설명하기](https://docs.docker.com/get-started/kube-deploy/#describing-apps-using-kubernetes-yaml)를 참조하세요.