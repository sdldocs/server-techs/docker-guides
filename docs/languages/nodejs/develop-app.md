## 전제 조건
[컨테이너로 이미지 실행](./run-containers.md)에서 이미지를 빌드하고 컨테이너화된 어플리케이션으로 이미지를 컨테이너로 실행 단계를 따르세요.

## 소개
이 모듈에서는 이전 모듈에서 빌드한 어플리케이션을 위한 로컬 개발 환경을 설정하는 과정을 살펴본다. Docker를 사용하여 이미지를 빌드하고 모든 작업을 훨씬 더 쉽게 수행할 수 있는 Docker Compose를 사용할 것이다.

## 로컬 데이터베이스와 컨테이너
먼저 컨테이너에서 데이터베이스를 실행하는 방법과 볼륨과 네트워킹을 사용하여 데이터를 지속시키고 어플리케이션이 데이터베이스와 통신할 수 있도록 하는 방법을 살펴보겠다. 그런 다음 모든 것을 하나의 명령으로 로컬 개발 환경을 설정하고 실행할 수 있는 Compose 파일로 모아본다. 마지막으로 컨테이너 내부에서 실행 중인 어플리케이션에 디버거를 연결하는 방법을 살펴보겠다.

MongoDB를 다운로드하고, 설치하고, 구성한 다음 서비스로서 Mongo 데이터베이스를 실행하는 대신, MongoDB용 Docker 공식 이미지를 사용하여 컨테이너에서 실행할 수 있다.

컨테이너에서 MongoDB를 실행하기 전에, 영구 데이터와 구성을 저장하기 위해 Docker가 관리할 수 있는 몇 개의 볼륨을 생성하려고 한다. 바인드 마운트를 사용하는 대신 도커가 제공하는 관리 볼륨 기능을 사용해 보겠다. 자세한 내용은 [Use volumes](https://docs.docker.com/storage/volumes/)을 참조하세요.

이제 볼륨을 생성해 보겠습니다. 데이터용 볼륨과 MongoDB 구성용 볼륨을 각각 하나씩 생성하겠다.

```bash
$ docker volume create mongodb
$ docker volume create mongodb_config
```

이제 어플리케이션과 데이터베이스가 서로 통신하는 데 사용할 네트워크를 만들자. 이 네트워크를 사용자 정의 브리지 네트워크(user-defined bridge network)라고 하며 연결 문자열을 만들 때 사용할 수 있는 멋진 DNS 조회 서비스를 제공한다.

```bash
$ docker network create mongodb
```

이제 컨테이너에서 MongoDB을 실행하고 위에서 생성한 볼륨과 네트워크에 연결할 수 있다. Docker는 Hub에서 이미지를 가져와 로컬에서 실행한다. 다음 명령에서 옵션 `-v`는 볼륨으로 컨테이너를 시작하기 위한 것이다. 자세한 내용은 [Docker 볼륨](https://docs.docker.com/storage/volumes/)을 참조하세요.

```bash
$ docker run -it --rm -d -v mongodb:/data/db \
  -v mongodb_config:/data/configdb -p 27017:27017 \
  --network mongodb \
  --name mongodb \
  mongo
```

이제 실행 중인 MongoDB가 있으므로, 인메모리 데이터 저장소가 아닌 MongoDB를 사용하도록 `server.js`를 업데이트한다.

```javascript
const ronin 		= require( 'ronin-server' )
const database  = require( 'ronin-database' )
const mocks 		= require( 'ronin-mocks' )

async function main() {

    try {
    await database.connect( process.env.CONNECTIONSTRING )
    
    const server = ronin.server({
            port: process.env.SERVER_PORT
        })

        server.use( '/', mocks.server( server.Router()) )

    const result = await server.start()
        console.info( result )
    
    } catch( error ) {
        console.error( error )
    }
}

main()
```

`ronin-database` 모듈을 추가하고 데이터베이스에 연결하고 인메모리 플래그를 false로 설정하도록 코드를 업데이트했다. 이제 변경 사항이 포함되도록 이미지를 다시 빌드해야 한다.

먼저 npm을 사용하여 어플리케이션에 `ronin-database` 모듈을 추가한다.

```bash
$ npm install ronin-database
```

이제 이미지를 빌드한다.

```bash
$ docker build --tag node-docker .
```

이제 컨테이너를 실행해 보자. 하지만 이번에는 어플리케이션이 데이터베이스에 액세스하는 데 사용할 연결 문자열을 알 수 있도록 `CONNECTIONSTRING` 환경 변수를 설정해야 한다. 이 작업은 `docker run` 명령에서 바로 수행할 수 있다.

```bash
$ docker run \
  -it --rm -d \
  --network mongodb \
  --name rest-server \
  -p 8000:8000 \
  -e CONNECTIONSTRING=mongodb://mongodb:27017/notes \
  node-docker
```

연결 문자열 끝에 있는 `notes`는 데이터베이스 이름이다.

어플리케이션이 데이터베이스에 연결되어 있고 메모를 추가할 수 있는지 테스트해 보자.

```bash
$ curl --request POST \
  --url http://localhost:8000/notes \
  --header 'content-type: application/json' \
  --data '{"name": "this is a note", "text": "this is a note that I wanted to take while I was working on writing a blog post.", "owner": "peter"}'
```

서비스로부터 다음과 같은 json을 돌려받을 것이다.

```
{"code":"success","payload":{"_id":"5efd0a1552cd422b59d4f994","name":"this is a note","text":"this is a note that I wanted to take while I was working on writing a blog post.","owner":"peter","createDate":"2020-07-01T22:11:33.256Z"}}
```

## Compose를 사용하여 로컬에서 개발
이 섹션에서는 하나의 명령으로 node-docker와 MongoDB를 시작하기 위해 Compose 파일을 생성하겠다. 또한, 실행 중인 node 프로세스에 디버거를 연결할 수 있도록 디버그 모드에서 node-docker를 시작하도록 Compose 파일을 설정하겠다.

IDE 또는 텍스트 편집기에서 notes-service를 열고 `docker-compose.dev.yml`이라는 이름의 새 파일을 만든다. 아래 명령을 복사하여 파일에 붙여넣는다.

```yml
version: '3.8'

services:
 notes:
  build:
   context: .
  ports:
   - 8000:8000
   - 9229:9229
  environment:
   - SERVER_PORT=8000
   - CONNECTIONSTRING=mongodb://mongo:27017/notes
  volumes:
   - ./:/app
   - /app/node_modules
  command: npm run debug

 mongo:
  image: mongo:4.2.8
  ports:
   - 27017:27017
  volumes:
   - mongodb:/data/db
   - mongodb_config:/data/configdb
volumes:
 mongodb:
 mongodb_config:
```

이 Compose 파일은 `docker run` 명령에 전달할 모든 매개 변수를 입력할 필요가 없으므로 매우 편리하다. Compose 파일을 사용하면 선언적으로 이를 수행할 수 있다.

디버거에 연결할 수 있도록 `port 9229`를 노출하고 있다. `volume`을 사용하면 로컬 소스 코드를 실행 중인 컨테이너에 매핑하여 텍스트 편집기에서 변경하고 컨테이너에서 해당 변경 사항을 가져올 수 있다.

Compose 파일 사용의 또 다른 멋진 기능 중 하나는 서비스 이름을 사용하도록 서비스 리솔루션(resolution)을 설정할 수 있다는 것이다. 따라서 이제 연결 문자열에 `"mongo"`를 사용할 수 있습니다. mongo를 사용하는 이유는 Compose 파일에서 MongoDB 서비스 이름을 그렇게 지정했기 때문이다.

디버그 모드로 어플리케이션을 시작하려면 `package.json` 파일에 한 줄을 추가하여 디버그 모드에서 애플리케이션을 시작하는 방법을 npm에 알려주어야 한다.

`package.json` 파일을 열고 스크립트 섹션에 다음 줄을 추가한다.

```yml
"debug": "nodemon --inspect=0.0.0.0:9229 -L server.js"
```

보시다시피, nodemon을 사용하려고 한다. nodemon은 서버를 디버그 모드로 시작하고 변경된 파일을 확인하며 서버를 재시작한다. 터미널에서 다음 명령을 실행하여 프로젝트 디렉토리에 nodemon을 설치한다.

```bash
$ npm install nodemon
```

어플리케이션을 시작하고 제대로 실행되는지 확인해 보자.

```bash
$ docker compose -f docker-compose.dev.yml up --build
```

`--build` 플래그를 전달하여 Docker가 이미지를 컴파일한 다음 컨테이너를 시작하도록 한다.

모든 것이 순조롭다면 아래와 비슷한 내용이 디스플레이될 것이다.

![]()

이제 API 엔드포인트를 테스트해 보겠다. 다음 curl 명령을 실행한다.

```bash
$ curl --request GET --url http://localhost:8000/notes
```

다음과 같은 응답을 받으실 수 있다.

```
{"code":"success","meta":{"total":0,"count":0},"payload":[]}
```

## 디버거 연결
Chrome 브라우저와 함께 제공되는 디버거를 사용하려고 한다. 컴퓨터에서 Chrome을 연 다음 주소창에 다음을 입력한다.

`about:inspect`

다음 화면이 열린다.

![](../../images/languages/chrome-inspect.png)

**Configure**을 선택한다. **Target discovery settings**이 열린다. 대상이 없는 경우 `127.0.0.1:9229`를 지정한 다음 **Done**을 선택한다.

**Open dedicated DevTools for Node**를 선택한다. 그러면 컨테이너 내부에서 실행 중인 Node.js 프로세스에 연결된 Devtools가 열린다.

소스 코드를 변경한 다음 breakpoint을 설정해 보자.

기존 `server.use()` 문 위에 다음 코드를 추가하고 파일을 저장한다. breakpoint을 적절히 설정할 수 있도록 `return` 문이 아래와 같이 한 줄에 있는지 확인하세요.

```js
server.use( '/foo', (req, res) => {
   return res.json({ "foo": "bar" })
 })
```

Compose 어플리케이션이 실행 중인 터미널을 살펴보면 nodemon이 변경 사항을 감지하고 어플리케이션을 다시 로드한 것을 확인할 수 있다.

![]()

다시 Chrome 개발자 도구로 이동하여 `return res.json({ "foo": "bar" })` 문이 포함된 줄에 breakpoint을 설정한 다음 아래 curl 명령을 실행하여 breakpoint을 트리거한다.

```bash
$ curl --request GET --url http://localhost:8000/foo
```

코드가 breakpoint에서 멈추는 것을 볼 수 있으며, 이제 평소와 마찬가지로 디버거를 사용할 수 있다. 변수를 검사하며, 관찰하고, 조건부 breakpoint을 설정하고, 스택 trace를 볼 수 있다.

## 다음 단계
이 모듈에서는 일반적인 명령어와 거의 비슷하게 사용할 수 있는 일반적인 개발 이미지를 만드는 방법을 살펴보았다. 또한 소스 코드를 실행 중인 컨테이너에 매핑하고 디버깅 포트를 노출하도록 Compose 파일을 설정하였다.

다음 모듈에서는 Docker에서 단위 테스트를 실행하는 방법을 살펴보도록 한다.
