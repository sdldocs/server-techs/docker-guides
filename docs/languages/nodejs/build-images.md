## 전제 조건
- 기본 [Docker 개념](../../get-started/overview.md)에 대한 이해
- [Dockerfile 형식](https://docs.docker.com/build/building/packaging/#dockerfile)에 익숙
- 머신에서 [BuildKit 활성화](https://docs.docker.com/build/buildkit/#getting-started)

## 개요
이제 컨테이너와 Docker 플랫폼에 대해 잘 살펴보았으니 첫 번째 이미지를 빌드하는 방법을 살펴보자. 이미지에는 코드 또는 바이너리, 런타임, 종속성, 기타 필요한 파일 시스템 객체 등 어플리케이션을 실행하는 데 필요한 모든 것이 포함된다.

이 튜토리얼을 완료하려면 다음이 필요하다.

- Node.js 버전 18 이상. [Node.js 다운로드](https://nodejs.org/en/)
- 로컬에서 실행 중인 Docker. 지침에 따라 [Docker를 다운로드하여 설치](https://docs.docker.com/desktop/)
- 파일을 편집하기 위한 IDE 또는 텍스트 편집기. [Visual Studio Code](https://code.visualstudio.com/Download)를 사용하는 것을 권장

## <a name="sample_application"></a>샘플 어플리케이션
예제로 사용할 수 있는 간단한 Node.js 어플리케이션을 만들어 보겠다. 로컬 머신에 node-docker라는 디렉터리를 만들고 아래 단계에 따라 간단한 REST API를 생성한다.

```bash
cd [path to your node-docker directory]
npm init -y
npm install ronin-server ronin-mocks
touch server.js
```

이제 REST 요청을 처리하는 코드를 추가한다. 어플리케이션을 도커화하는 데 집중할 수 있도록 모의 서버를 사용하겠다.

IDE에서 이 작업 디렉터리를 열고 `server.js` 파일에 다음 코드를 추가한다.

```javascript
const ronin = require('ronin-server')
const mocks = require('ronin-mocks')

const server = ronin.server()

server.use('/', mocks.server(server.Router(), false, true))
server.start()
```

모의 서버는 `Ronin.js`라고 불리며 기본적으로 포트 8000에서 수신 대기한다. 루트(/) 엔드포인트로 POST 요청을 보낼 수 있으며 서버로 보내는 모든 JSON 구조는 메모리에 저장된다. 또한 동일한 엔드포인트로 GET 요청을 보내 이전에 POST된 JSON 객체 배열을 받을 수도 있다.

## 어플리케리션 테스트
어플리케이션을 시작하고 실행 중인지 확인한다. 터미널을 열고 생성한 작업 디렉토리로 이동한다.

```bash
$ node server.js
```

어플리케이션이 작동하는지 테스트하려면 API에 일부 JSON 데이터를 포함한 POST 요청을 한 다음 GET 요청을 하여 데이터가 저장되었는지 확인한다.

새 터미널을 열고 다음 curl 명령을 실행한다.

```bash
$ curl --request POST \
  --url http://localhost:8000/test \
  --header 'content-type: application/json' \
  --data '{"msg": "testing" }'
```

POST 요청이 성공하면 출력은 다음과 비슷하게 디스플레이될 것이다.

```
{"code":"success","payload":[{"msg":"testing","id":"31f23305-f5d0-4b4f-a16f-6f4c8ec93cf1",
"createDate":"2020-08-28T21:53:07.157Z"}]}
```

이제 GET 요청을 한다.

```
$ curl http://localhost:8000/test
```

출력은 다음과 비슷하게 디스플레이될 것이다.

```
{"code":"success","meta":{"total":1,"count":1},"payload":[{"msg":"testing","id":"31f23305-f5d0-4b4f-a16f-6f4c8ec93cf1",
"createDate":"2020-08-28T21:53:07.157Z"}]}
```

서버가 실행 중인 터미널로 다시 전환한다. 이제 서버 로그에 다음과 같은 요청이 디스플레이된다.

```
2020-XX-31T16:35:08:4260  INFO: POST /test
2020-XX-31T16:35:21:3560  INFO: GET /test
```

Great! 어플리케이션이 작동하는지 확인했다. 이 단계에서는 로컬에서 서버 스크립트 테스트를 완료한 것이다.

서버가 실행 중인 터미널 세션에서 `CTRL-c`를 눌러 서버를 중지한다.

```
2021-08-06T12:11:33:8930  INFO: POST /test
2021-08-06T12:11:41:5860  INFO: GET /test
^Cshutting down...
```

이제 Docker에서 어플리케이션을 계속 빌드하고 실행하겠다.

## Node.js을 위한 Dockerfile 만들기
다음으로, 어플리케이션에 사용할 기본 이미지를 도커에게 알려주는 명령을 Dockerfile에 추가해야 한다.

```dockerfile
# syntax=docker/dockerfile:1

FROM node:latest
```

Docker 이미지는 다른 이미지에서 상속할 수 있습니다. 따라서 자체 기본 이미지를 생성하는 대신 Node.js 어플리케이션을 실행하는 데 필요한 모든 도구와 패키지가 이미 포함된 공식 Node.js 이미지를 사용하겠다. 객체 지향 프로그래밍에서 클래스 상속을 생각할 때와 같은 방식으로 생각하면 된다. 예를 들어 JavaScript로 Docker 이미지를 만들 수 있다면 다음과 같이 작성할 수 있다.

```javascript
class MyImage extends NodeBaseImage {}
```

이렇게 하면 기본 클래스 `NodeBaseImage`로부터 기능을 상속받은 `MyImage`라는 클래스가 생성된다.

같은 방식으로 `FROM` 명령을 사용할 때 Docker에게 `node:latest` 이미지의 모든 기능을 이미지에 포함하도록 지시한다.

> **Note**:
>
> 나만의 기본 이미지를 만드는 방법에 대해 자세히 알아보려면 [Creating base images](https://docs.docker.com/build/building/base-images/)를 참조하세요.

`NODE_ENV` 환경 변수는 애플리케이션이 실행 중인 환경(일반적으로 개발 또는 프로덕션)을 지정한다. 성능을 개선하기 위해 할 수 있는 가장 간단한 방법 중 하나는 `NODE_ENV`를 `production`으로 설정하는 것이다.

```dockerfile
ENV NODE_ENV=production
```

나머지 명령을 실행할 때 조금 더 쉽게 작업할 수 있도록 작업 디렉터리를 생성한다. 이렇게 하면 Docker가 이 경로를 이후의 모든 명령에 대하여 기본 위치로 사용하도록 지시하는 것이다. 즉, 전체 파일 경로 대신 작업 디렉터리를 기준으로 상대 파일 경로를 사용할 수 있다.

```dockerfile
WORKDIR /app
```

일반적으로 Node.js로 작성된 프로젝트를 다운로드한 후 가장 먼저 하는 일은 npm 패키지를 설치하는 것이다. 이렇게 하면 어플리케이션의 모든 종속 요소가 Node 런타임이 찾을 수 있는 `node_modules` 디렉터리에 설치된다.

`npm install`을 실행하기 전에 `package.json`과 `package-lock.json` 파일을 이미지로 가져와야 한다. 이를 위해 `COPY` 명령을 사용한다. `COPY` 명령에는 두 개의 매개변수, 즉 `src`와 `dest`이 필요하다. 첫 번째 매개변수인 `src`는 이미지에 복사할 파일을 Docker에 알려준다. 두 번째 매개변수 `dest`는 해당 파일을 복사할 위치를 Docker에 알려준다. 예를 들어

```dockerfile
COPY ["<src>", "<dest>"]
```

쉼표로 구분하여 여러 개의 `src` 리소스를 지정할 수 있다. 예를 들면 `COPY ["<src1>", "<src2>",..., "<dest>"]`이다. `package.json`과 `package-lock.json` 파일을 작업 디렉토리 `/app`에 복사한다.

```dockerfile
COPY ["package.json", "package-lock.json*", "./"]
```

전체 작업 디렉터리를 복사하는 것이 아니라 `package.json` 파일만 복사한다는 점에 유의하세요. 이렇게 하면 캐시된 Docker 레이어를 활용할 수 있다. 이미지 안에 파일이 있으면 `RUN` 명령을 사용하여 `npm install` 명령을 실행할 수 있다. 이 명령은 머신에서 로컬에서 `npm install`을 실행하는 것과 똑같이 작동하지만, 이번에는 이미지 내부의 `node_modules` 디렉터리에 노드 모듈이 설치된다.

```dockerfile
RUN npm install --production
```

이 시점에서 node 최신 버전을 기반으로 하는 이미지가 있고 종속성을 설치했다. 다음으로 해야 할 일은 이미지에 소스 코드를 추가하는 것이다. 위의 `package.json` 파일에서 했던 것처럼 COPY 명령을 사용한다.

```dockerfile
COPY . .
```

COPY 명령은 현재 디렉토리에 있는 모든 파일을 가져와 이미지에 복사한다. 이제 컨테이너 내부에서 이미지가 실행될 때 어떤 명령을 실행할지 Docker에 알려주기만 하면 된다. 이 작업은 `CMD` 명령으로 수행한다.

```dockerfile
CMD ["node", "server.js"]
```

전체 Dockerfile은 다음과 같다.

```dockerfile
# syntax=docker/dockerfile:1

FROM node:18-alpine
ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

CMD ["node", "server.js"]
```

## `.dockerignore` 파일 생성
[build context](https://docs.docker.com/build/building/context/)에서 파일을 사용하려면 Dockerfile은 명령어(예: COPY 명령어)에 지정된 파일을 참조한다. `.dockerignore` 파일을 사용하면 빌드 컨텍스트(build context)에서 제외할 파일과 디렉터리를 지정할 수 있다. 빌드 성능을 개선하려면 `.dockerignore` 파일을 생성하고 그 안에 `node_modules` 디렉터리를 추가한다.

```
node_modules
```

## 이미지 빌드
이제 Dockerfile을 만들었으니 이미지를 빌드해 보자. 이를 위해 `docker build` 명령을 사용한다. `docker build` 명령은 Dockerfile과 "컨텍스트"로 부터 Docker 이미지를 빌드한다. 빌드의 컨텍스트는 지정된 PATH 또는 URL에 있는 파일 집합이다. Docker 빌드 프로세스는 이 컨텍스트에 있는 모든 파일에 액세스할 수 있다.

빌드 명령은 선택적으로 `--tag` 플래그를 사용할 수 있다. 이 태그는 이미지의 이름과 선택적 태그를 `name:tag` 형식으로 설정한다. 작업을 단순화하기 위해 지금은 선택적 `tag`를 생략한다. 태그를 전달하지 않으면 Docker는 "latest"을 기본 태그로 사용한다.

Docker 이미지를 빌드한다.

```bash
$ docker build --tag node-docker .
```

```
[+] Building 93.8s (11/11) FINISHED
 => [internal] load build definition from dockerfile                                          0.1s
 => => transferring dockerfile: 617B                                                          0.0s
 => [internal] load .dockerignore                                                             0.0s
 ...
 => [2/5] WORKDIR /app                                                                        0.4s
 => [3/5] COPY [package.json, package-lock.json*, ./]                                         0.2s
 => [4/5] RUN npm install --production                                                        9.8s
 => [5/5] COPY . .
```

## 로컬 이미지 보기
로컬 머신에 있는 이미지 목록을 보려면 두 가지 옵션이 있다. 하나는 Docker CLI를 사용하는 것이고 다른 하나는 Docker 데스크톱을 사용하는 것이다. 이미 터미널에서 작업하고 있으므로 CLI를 사용하여 이미지를 나열하는 방법을 살펴보.

이미지를 나열하려면 `docker images` 명령을 실행한다.

```bash
$ docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
node-docker         latest              3809733582bc        About a minute ago   945MB
```

정확한 출력은 다를 수 있지만, `latest` 태그를 사용하는 방금 빌드한 `node-docker:latest` 이미지가 있어야 한다.

## 이미지 태그하기
앞서 언급했듯이 이미지 이름은 슬래시로 구분된 이름들로 구성된다. 이름 구성 요소에는 소문자, 숫자, 구분 기호가 포함될 수 있다. 구분 기호에는 마침표, 밑줄 하나 또는 두 개, 대시 하나 이상이 포함될 수 있다. 이름 구성 요소는 구분 기호로 시작하거나 구분 기호로 끝날 수 없다.

이미지는 매니페스트와 레이어 목록으로 구성된다. '태그'가 이러한 아티팩트의 조합을 가리키는 것 외에는 이 시점에서 매니페스트와 레이어에 대해 너무 걱정하지 마세요. 이미지에 여러 개의 태그를 사용할 수 있다. 작성한 이미지에 대한 두 번째 태그를 만들고 해당 레이어를 살펴보겠다.

작성한 이미지에 대한 새 태그를 만들려면 다음 명령을 실행한다.

```bash
$ docker tag node-docker:latest node-docker:v1.0.0
```

`docker tag` 명령은 이미지에 대한 새 태그를 만든다. 새 이미지를 만들지는 않는다. 태그는 동일한 이미지를 가리키며 이미지를 참조하는 또 다른 방법일 뿐이다.

이제 `docker images` 명령을 실행하여 로컬 이미지 목록을 확인한다.

```bash
$ docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
node-docker         latest              3809733582bc        24 minutes ago      945MB
node-docker         v1.0.0              3809733582bc        24 minutes ago      945MB
```

두 이미지가 `node-docker`로 시작하는 것을 볼 수 있다. `IMAGE ID` 열을 살펴보면 두 이미지의 값이 동일하다는 것을 알 수 있으므로 같은 이미지임을 알 수 있다.

방금 만든 태그를 제거하자. 이렇게 하려면 `rmi` 명령을 사용한다. `rmi` 명령은 이미지 제거를 의미한다.

```bash
$ docker rmi node-docker:v1.0.0
```

```
Untagged: node-docker:v1.0.0
```

Docker의 응답을 보면 Docker가 이미지를 제거한 것이 아니라 "태그가 제거"된 것임을 알 수 있다. 이를 확인하려면 `docker images` 명령을 실행하면 된다.

```bash
$ docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
node-docker         latest              3809733582bc        32 minutes ago      945MB
```

Docker에서 `:v1.0.0`으로 태그가 지정된 이미지를 제거했지만 사용자 컴퓨터에서 `python-docker:latest` 태그를 사용할 수 있다.

## 다음 단계
이 모듈에서는 이후 튜토리얼에 사용되는 Node 어플리케이션의 예을 설정하고, Docker 이미지를 빌드하는 데 사용되는 Dockerfile을 생성하는 방법을 살펴보았다. 또한 이미지에 태그를 지정하고 제거하는 방법도 살펴보았다.

다음 모듈에서는 [이미지를 컨테이너로 실행하는 방법](run-containers.md)을 살펴보겠다.
