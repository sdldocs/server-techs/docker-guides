## Github Actions 시작하기
이 튜토리얼에서는 Docker 이미지를 빌드하고 이를 Docker Hub로 푸시하기 위해 Docker GitHub 작업을 설정하고 사용하는 프로세스를 안내한다. 다음 단계로 완료할 것이다.

1. GitHub에 새 리포지토리를 만든다.
2. GitHub Actions 워크플로우를 정의한다.
3. 워크플로우를 실행한다.

이 튜토리얼을 따라 하려면 Docker ID와 GitHub 계정이 필요하다.

### Step one: 리포지토리 생성
GitHub 리포지토리를 만들고 Docker Hub 시크릿을 구성한다.

<span>1.</span> [이 template repository](https://github.com/dvdksn/clockbox/generate)를 사용하여 새 GitHub 리포지토리를 만든다.

리포지토리에는 간단한 Dockerfile만 포함되어 있다. 원하는 경우 작동하는 Dockerfile이 포함된 다른 리포지토리를 자유롭게 사용할 수 있다.

<span>2.</span> 리포지토리 **Settings**을 열고 **Secrets and variables > Actions**으로 이동한다.

<span>3.</span> DOCKERHUB_USERNAME이라는 이름의 새 secret을 생성하고 값으로 Docker ID를 사용한다.

<span>4.</span> Docker Hub용 새 [Personal Access Token (PAT)](https://docs.docker.com/docker-hub/access-tokens/#create-an-access-token)을 생성한다. 이 토큰의 이름을 `clockboxci`로 지정할 수 있다.

<span>5.</span> GitHub 리포지토리에 두 번째 secret으로 `DOCKERHUB_TOKEN`이라는 이름으로 PAT를 추가한다.

리포지토리를 생성하고 secret을 구성했으면 이제 실행할 준비가 되었다!

### Step two: 워크프로우 설정
이미지를 빌드하고 Docker Hub에 푸시하기 위한 GitHub 작업 워크플로우를 설정한다.

<span>1.</span> GitHub의 리포지토리로 이동한 다음 **Actions** 탭을 선택한다.

<span>2.</span> **set up a workflow yourself**를 선택한다.

그러면 리포지토리에 새 GitHub actions 워크플우로 파일을 만들기 위한 기본적으로 `.github/workflows/main.yml` 아래에 있는 페이지로 이동한다.

<span>3.</span> 편집기 창에서 다음 YAML 구성을 복사하여 붙여넣는다.

```yml
name: ci

on:
  push:
    branches:
      - "main"

jobs:
  build:
    runs-on: ubuntu-latest
```

    - `name`: 이 워크플로의 이름
    - `on.push.branch`: 목록에 있는 브랜치에 대한 모든 푸시 이벤트에서 이 워크플로우를 실행하도록 지정
    - `jobs`: job ID(`build`)를 만들고 작업이 실행되어야 하는 컴퓨터 유형을 선언한다.

여기서 사용되는 YAML 구문에 대한 자세한 내용은 [GitHub Actions을 위한 워크플로우 구문](https://docs.github.com/en/actions/using-workflows/workflow-syntax-for-github-actions)을 참조하세요.

### Step three: 워크플로우 단계 
이제 필수 사항: 실행할 단계와 실행 순서이다.

```yml
jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      -
        name: Checkout
        uses: actions/checkout@v3
      -
        name: Login to Docker Hub
        uses: docker/login-action@v2
        with:
          username: ${{ secrets.DOCKERHUB_USERNAME }}
          password: ${{ secrets.DOCKERHUB_TOKEN }}
      -
        name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2
      -
        name: Build and push
        uses: docker/build-push-action@v4
        with:
          context: .
          file: ./Dockerfile
          push: true
          tags: ${{ secrets.DOCKERHUB_USERNAME }}/clockbox:latest
```

이전 YAML 스니펫에는 다음과 같은 일련의 단계가 포함되어 있다.

1. 빌드 머신에서 리포지토리를 체크아웃한다.
2. [Docker Login](https://github.com/marketplace/actions/docker-login) action과 Docker Hub 자격 증명을 사용하여 Docker Hub에 로그인한다.
3. [Docker Setup Buildx](https://github.com/marketplace/actions/docker-setup-buildx) action을 사용하여 BuildKit 빌더 인스턴스를 만든다.
4. [Docker 이미지 빌드 및 푸시를 사용](https://github.com/marketplace/actions/build-and-push-docker-images)하여 컨테이너 이미지를 빌드하고 Docker Hub 리포지토리에 푸시한다.

    `with` 키는 단계를 구성하는 여러 입력 매개변수를 나열한다.

    - `context`: [빌드 컨텍스트](https://docs.docker.com/build/building/context/)
    - `file`: Dockerfile의 파일 경로.
    - `push`: 이미지를 빌드한 후 레지스트리에 업로드하도록 작업에 지시
    - `tags`: 이미지를 푸시할 위치를 지정하는 태그

워크플로우 파일에 이러한 단계를 추가한다. 전체 워크플로우 구성은 다음과 같아야 한다.

```yml
name: ci

on:
  push:
    branches:
      - "main"

jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      -
        name: Checkout
        uses: actions/checkout@v3
      -
        name: Login to Docker Hub
        uses: docker/login-action@v2
        with:
          username: ${{ secrets.DOCKERHUB_USERNAME }}
          password: ${{ secrets.DOCKERHUB_TOKEN }}
      -
        name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2
      -
        name: Build and push
        uses: docker/build-push-action@v4
        with:
          context: .
          file: ./Dockerfile
          push: true
          tags: ${{ secrets.DOCKERHUB_USERNAME }}/clockbox:latest
```

### 워크플로우 실행
워크플로우 파일을 저장하고 작업을 실행한다.

1. **Commit changes...**을 선택하고 변경사항을 `main` 브랜치에 푸시한다.

    커밋을 푸시하면 워크플로우를 자동으로 시작한다.

2. **Actions** 탭으로 이동한다. 워크플로우가 표시된다.

    워크플로우를 선택하면 모든 단계의 내역을 보여준다.

3. 워크플로우가 완료되면 Docker Hub의 리포지토리로 이동한다.

    목록에 새 리포지토리가 표시되면 GitHub actions이 이미지를 Docker Hub에 성공적으로 푸시했다는 뜻이다!

## 다음 단계
이 Part에서는 기존 Docker 프로젝트에 GitHub Actions 워크플로우를 설정하고, 워크플로우를 최적화하여 빌드 시간을 개선하고 풀 리퀘스트 횟수를 줄이는 방법을 배웠으며, 마지막으로 특정 버전만 Docker Hub에 푸시하는 방법을 배웠다. 또한 최신 태그에 대해 야간 테스트를 설정하거나, 각 PR을 테스트하거나, 사용 중인 태그에 대해 더 우아한 작업을 수행하고 이미지에서 동일한 태그에 대해 Git 태그를 사용할 수 있다.

어플리케이션 배포를 고려할 수도 있다.
