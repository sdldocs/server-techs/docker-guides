언어별 시작 가이드는 개발 환경을 설정하는 과정을 안내하고 Docker를 사용하여 언어별 어플리케이션의 컨테이너화를 시작한다. 학습 모듈에는 선호하는 언어로 새 Dockerfile을 만드는 방법, Docker 이미지에 포함할 내용, Docker 이미지를 개발하고 실행하는 방법, CI/CD 파이프라인을 설정하는 방법, 마지막으로 개발한 어플리케이션을 Cloud로 푸시하는 방법에 대한 정보를 제공하는 모범 사례와 지침이 포함되어 있다.

언어별 모듈 외에도 Docker 문서는 개발 환경을 구축하고 효율적으로 관리하기 위한 가이드라인도 제공한다. Dockerfile 작성, 효율적인 이미지 빌드와 관리, BuildKit을 사용하여 이미지 빌드를 통한 성능 향상 등에 대한 모범 사례에 대한 정보를 찾을 수 있다. 또한 이미지를 작게 유지하는 방법, 어플리케이션 데이터를 유지하는 방법, 다단계 빌드 사용 방법 등에 대한 구체적인 지침도 찾을 수 있다.

자세한 내용은 다음 주제를 참조하세요.

- Dockerfile 작성을 위한 모범 사례
- Docker 개발 모범 사례
- BuildKit으로 이미지 빌드

## 언어별 시작 가이드
Docker 환경을 설정하고 애플리케이션 컨테이너화를 시작하는 방법을 알아본다. 아래에서 언어를 선택하여 시작하세요. (현재 프로젝트에 필요한 언어만 먼저 편역하였음)

[![node.js](../images/languages/nodejs.png)](../languages/nodejs/overview.md)

[![python](../images/languages/python.png)](../languages/python/overview.md)

[![golang](../images/languages/golang.png)](../languages/golang/overview.md)
