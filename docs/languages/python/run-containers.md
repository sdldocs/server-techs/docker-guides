## 전제 조건
[Python 이미지 빌드](build-images.md) Python 이미지 빌드  단계를 수행한다.

## 개요
이전 모듈에서 샘플 어플리케이션을 생성한 다음 이미지를 생성하는 데 사용할 Dockerfile을 만들었다. 도커 명령인 `docker build`를 사용하여 이미지를 생성했다. 이제 이미지가 생겼으니 해당 이미지를 실행하여 어플리케이션이 정확히 실행되는지 확인할 수 있다.

컨테이너는 호스트와 분리된 자체 파일 시스템, 자체 네트워킹, 자체 격리 프로세스 트리를 가지고 있다는 점에서 격리되어 있다는 점을 제외하면 일반적인 운영 체제 프로세스와 유사하다.

컨테이너 내부에서 이미지를 실행하려면 `docker run` 명령을 사용한다. `docker run` 명령에는 이미지의 이름인 하나의 매개변수가 필요하다. 이미지를 시작하고 올바르게 실행되는지 확인해 보자. 터미널에서 다음 명령을 실행한다.

```bash
$ docker run python-docker
```

이 명령을 실행한 후 명령 프롬프트로 돌아가지 않는 것을 확인할 수 있다. 이는 어플리케이션이 REST 서버이며 컨테이너를 중지할 때까지 제어권을 OS로 되돌리지 않고 들어오는 요청을 기다리는 루프에서 실행되기 때문이다.

새 터미널을 열고 `curl` 명령을 사용하여 서버에 `GET` 요청을 해보자.

```bash
$ curl localhost:5000
curl: (7) Failed to connect to localhost port 5000: Connection refused
```

보시다시피 서버에 대한 연결이 거부되어 `curl` 명령이 실패했다. 즉, 포트 5000에서 로컬 호스트에 연결할 수 없었다. 이는 컨테이너가 네트워킹을 포함하여 격리된 상태로 실행되고 있기 때문에 예상되는 현상이다. 컨테이너를 중지하고 로컬 네트워크에 게시된 포트 5000으로 다시 시작하겠다.

컨테이너를 중지하려면 `ctrl-C`를 누른다. 그러면 터미널 프롬프트로 돌아간다.

컨테이너의 포트를 게시하려면 `docker run` 명령에 `--publish` 플래그(줄여서 `-p`)를 사용한다. `--publ;ish` 명령의 형식은 `[host port]:[container port]`이다. 따라서 컨테이너 내부의 포트 5000을 컨테이너 외부의 포트 3000에 노출하려면 `--publish` 플래그에 `3000:5000`을 전달하면 된다.

컨테이너에서 플라스크 어플리케이션을 실행할 때 포트를 지정하지 않았으므로 기본값은 5000이다. 포트 5000으로 가는 이전 요청이 작동하도록 하려면 호스트의 포트 8000을 컨테이너의 포트 5000에 매핑하면 된다.

```bash
$ docker run --publish 8000:5000 python-docker
```

이제 위에서 `curl` 명령을 다시 실행해 보자. 새 터미널을 여는 것을 잊지 마세요.

```bash
$ curl localhost:8000
Hello, Docker!
```

성공! 포트 8000에서 컨테이너 내부에서 실행 중인 어플리케이션에 연결할 수 있었다. 컨테이너가 실행 중인 터미널로 다시 전환하면 콘솔에 `GET` 요청이 기록된 것을 볼 수 있다.

```
[31/Jan/2021 23:39:31] "GET / HTTP/1.1" 200 -
```

컨테이너를 중지하려면 `ctrl-c`를 누른다.

## 분리 모드에서 실행
지금까지는 훌륭하지만 샘플 어플리케이션은 웹 서버이므로 컨테이너에 연결할 필요가 없다. Docker는 컨테이너를 분리 모드 또는 백그라운드에서 실행할 수 있다. 이를 위해 `--detach` 또는 줄여서 `-d`를 사용할 수 있다. Docker는 이전과 동일하게 컨테이너를 시작하지만 이번에는 컨테이너에서 "분리"하고 터미널 프롬프트로 돌아간다.

```bash
$ docker run -d -p 8000:5000 python-docker
ce02b3179f0f10085db9edfccd731101868f58631bdf918ca490ff6fd223a93b
```

Docker가 백그라운드에서 컨테이너를 시작하고 터미널에 컨테이너 ID를 출력했다.

다시 한 번 컨테이너가 제대로 실행되고 있는지 확인해 보자. 위에서와 동일한 `curl` 명령을 실행한다.

```bash
$ curl localhost:8000
Hello, Docker!
```

## 컨테이너 나열
컨테이너를 백그라운드에서 실행했으니, 우리 컨테이너가 실행 중인지 또는 우리 컴퓨터에서 다른 컨테이너가 실행 중인지 어떻게 알 수 있을까? 컴퓨터에서 실행 중인 컨테이너 목록을 보려면 `docker ps`를 실행하면 된다. 이는 Linux 머신에서 프로세스 목록을 볼 때 `ps` 명령을 사용하는 방법과 유사하다.

```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
ce02b3179f0f        python-docker         "python3 -m flask ru…"   6 minutes ago       Up 6 minutes        0.0.0.0:8000->5000/tcp   wonderful_kalam
```

`docker ps` 명령은 실행 중인 컨테이너에 대한 다양한 정보를 제공한다. 컨테이너 ID, 컨테이너 내부에서 실행 중인 이미지, 컨테이너를 시작하는 데 사용된 명령, 컨테이너가 생성된 시기, 상태, 노출된 포트, 컨테이너 이름 등을 확인할 수 있다.

컨테이너의 이름이 어디서 유래했는지 궁금하실 것이다. 컨테이너를 시작할 때 컨테이너의 이름을 제공하지 않았기 때문에 Docker가 임의의 이름을 생성했다. 이 문제는 잠시 후에 해결하겠지만, 먼저 컨테이너를 중지해야 한다. 컨테이너를 중지하려면 컨테이너를 중지하는 `docker stop` 명령을 실행하면 된다. 컨테이너의 이름을 전달하거나 컨테이너 ID를 사용할 수 있다.

```bash
$ docker stop wonderful_kalam
wonderful_kalam
```

이제 `docker ps` 명령을 다시 실행하여 실행 중인 컨테이너 목록을 확인한다.

```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

## <a name="stop-start-and-name-containers"></a>컨테이너 정지, 시작과 명명
Docker 컨테이너를 시작, 중지, 다시 시작할 수 있다. 컨테이너 실행을 중지하면 컨테이너가 제거되지는 않지만 상태가 중지됨으로 변경되고 컨테이너 내부의 프로세스가 중지된다. 이전 모듈에서 `docker ps` 명령을 실행하면 기본 출력에는 실행 중인 컨테이너만 표시된다. `--all` 또는 `-a`를 전달하면 시작 또는 중지 상태에 관계없이 머신의 모든 컨테이너를 볼 수 있다.

```bash
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
ce02b3179f0f        python-docker         "python3 -m flask ru…"   16 minutes ago      Exited (0) 5 minutes ago                        wonderful_kalam
ec45285c456d        python-docker         "python3 -m flask ru…"   28 minutes ago      Exited (0) 20 minutes ago                       agitated_moser
fb7a41809e5d        python-docker         "python3 -m flask ru…"   37 minutes ago      Exited (0) 36 minutes ago                       goofy_khayyam
```

이제 여러 개의 컨테이너가 나열된 것을 볼 수 있다. 이러한 컨테이너를 시작하였고, 중지하였지만 제거하지 않은 컨테이너이다.

방금 중지한 컨테이너를 다시 시작하겠다. 방금 중지한 컨테이너의 이름을 찾아 아래 재시작 명령에서 컨테이너의 이름으로 바꾼다.

```bash
$ docker restart wonderful_kalam
```

이제 `docker ps` 명령을 사용하여 모든 컨테이너를 다시 나열한다.

```bash
$ docker ps --all
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS                    NAMES
ce02b3179f0f        python-docker         "python3 -m flask ru…"   19 minutes ago      Up 8 seconds                0.0.0.0:8000->5000/tcp   wonderful_kalam
ec45285c456d        python-docker         "python3 -m flask ru…"   31 minutes ago      Exited (0) 23 minutes ago                            agitated_moser
fb7a41809e5d        python-docker         "python3 -m flask ru…"   40 minutes ago      Exited (0) 39 minutes ago                            goofy_khayyam
```

방금 재시작한 컨테이너가 분리 모드로 시작되었고 포트 8000이 노출되어 있는 것을 확인할 수 있다. 또한 컨테이너의 상태가 "Up X seconds"인 것을 확인할 수 있다. 컨테이너를 다시 시작하면 원래 시작했을 때와 동일한 플래그 또는 명령으로 시작된다.

이제 모든 컨테이너를 중지하고 제거한 후 임의 이름 지정 문제를 해결해 보겠다. 방금 시작한 컨테이너를 중지한다. 실행 중인 컨테이너의 이름을 찾아 아래 명령의 이름을 시스템의 컨테이너 이름으로 바꾼다.

```bash
$ docker stop wonderful_kalam
wonderful_kalam
```

이제 모든 컨테이너가 중지되었으므로 컨테이너를 제거해 보자. 컨테이너를 제거하면 더 이상 실행 중이거나 중지된 상태가 아니며 컨테이너 내부의 프로세스가 중지되고 컨테이너의 메타데이터가 제거된다.

```bash
$ docker ps --all
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
ce02b3179f0f        python-docker         "python3 -m flask ru…"   19 minutes ago      Exited (0) 5 seconds ago                        wonderful_kalam
ec45285c456d        python-docker         "python3 -m flask ru…"   31 minutes ago      Exited (0) 23 minutes ago                       agitated_moser
fb7a41809e5d        python-docker         "python3 -m flask ru…"   40 minutes ago      Exited (0) 39 minutes ago                       goofy_khayyam
```

컨테이너를 제거하려면 컨테이너 이름과 함께 `docker rm` 명령을 실행한다. 하나의 명령에 여러 컨테이너 이름을 전달할 수 있습니다. 다시 말하지만, 다음 명령의 컨테이너 이름을 시스템의 컨테이너 이름으로 바꾸세요.

```bash
$ docker rm wonderful_kalam agitated_moser goofy_khayyam
wonderful_kalam
agitated_moser
goofy_khayyam
```

`docker ps --all` 명령을 다시 실행하여 모든 컨테이너가 제거되었는지 확인한다.

이제 임의의 이름 지정 문제를 해결해 보겠다. 표준 관행은 컨테이너에서 실행 중인 항목과 컨테이너가 연결된 어플리케이션 또는 서비스를 식별하기 쉽다는 간단한 이유 때문에 컨테이너 이름을 지정하는 것이다.

컨테이너 이름을 지정하려면 `docker run` 명령에 `--name` 플래그를 전달하기만 하면 된다.

```bash
$ docker run -d -p 8000:5000 --name rest-server python-docker
1aa5d46418a68705c81782a58456a4ccdb56a309cb5e6bd399478d01eaa5cdda
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
1aa5d46418a6        python-docker         "python3 -m flask ru…"   3 seconds ago       Up 3 seconds        0.0.0.0:8000->5000/tcp   rest-server
```

더 좋아졌다! 이제 이름만 보고도 컨테이너를 쉽게 식별할 수 있다.

## 다음 단계
이 모듈에서는 컨테이너 실행, 포트 게시, 분리된 모드에서 컨테이너 실행에 대해 살펴보았다. 또한 컨테이너를 시작, 중지, 재시작하여 컨테이너를 관리하는 방법도 살펴봤다. 또한 컨테이너를 더 쉽게 식별할 수 있도록 컨테이너 이름을 지정하는 방법도 살펴봤다. 다음 모듈에서는 컨테이너에서 데이터베이스를 실행하고 어플리케이션에 연결하는 방법을 알아보겠다.