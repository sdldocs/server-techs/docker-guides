## 전제 조건
[컨테이너로 이미지 실행](./run-containers.md)에서 이미지를 빌드하고 컨테이너화된 어플리케이션으로 이미지를 컨테이너로 실행 단계를 따르세요.

## 소개
이 모듈에서는 이전 모듈에서 빌드한 어플리케이션을 위한 로컬 개발 환경을 설정하는 과정을 살펴본다. Docker를 사용하여 이미지를 빌드하고 모든 작업을 훨씬 더 쉽게 수행할 수 있는 Docker Compose를 사용할 것이다.

## 컨테이너에서 데이터베이스 실행
먼저 컨테이너에서 데이터베이스를 실행하는 방법과 볼륨과 네트워킹을 사용하여 데이터를 지속시키고 어플리케이션이 데이터베이스와 통신할 수 있도록 하는 방법을 살펴보겠다. 그런 다음 모든 것을 하나의 명령으로 로컬 개발 환경을 설정하고 실행할 수 있는 Compose 파일로 모아본다.

MySQL을 다운로드하고, 설치하고, 구성한 다음, MySQL 데이터베이스를 서비스로 실행하는 대신, MySQL용 Docker 공식 이미지를 사용하여 컨테이너에서 실행할 수 있다.

컨테이너에서 MySQL을 실행하기 전에 Docker가 영구 데이터와 구성을 저장하기 위해 관리할 수 있는 몇 개의 볼륨을 생성하겠다. 바인드 마운트를 사용하는 대신 Docker가 제공하는 관리 볼륨 기능을 사용한다. [볼륨 사용](https://docs.docker.com/storage/volumes/)에 대한 모든 내용은 설명서에서 확인할 수 있다.

이제 볼륨을 생성해 보자. 데이터용 볼륨과 MySQL 구성용 볼륨을 각각 하나씩 생성하겠다.

```bash
$ docker volume create mysql
$ docker volume create mysql_config
```

이제 어플리케이션과 데이터베이스가 서로 통신하는 데 사용할 네트워크를 만들자. 이 네트워크를 사용자 정의 브리지 네트워크(user-defined bridge network)라고 하며 연결 문자열을 만들 때 사용할 수 있는 멋진 DNS 조회 서비스를 제공한다.

```bash
$ docker network create mysqlnet
```

이제 컨테이너에서 MySQL을 실행하고 위에서 생성한 볼륨과 네트워크에 연결할 수 있다. Docker는 Hub에서 이미지를 가져와 로컬에서 실행한다. 다음 명령에서 옵션 `-v`는 볼륨으로 컨테이너를 시작하기 위한 것이다. 자세한 내용은 [Docker 볼륨](https://docs.docker.com/storage/volumes/)을 참조하세요.

```bash
$ docker run --rm -d -v mysql:/var/lib/mysql \
  -v mysql_config:/etc/mysql -p 3306:3306 \
  --network mysqlnet \
  --name mysqldb \
  -e MYSQL_ROOT_PASSWORD=p@ssw0rd1 \
  mysql
```

이제 MySQL 데이터베이스가 실행 중이고 연결할 수 있는지 확인해 보자. 다음 명령을 사용하여 컨테이너 내부에서 실행 중인 MySQL 데이터베이스에 연결하고 비밀번호를 묻는 메시지가 표시되면 "p@ssw0rd1"을 입력한다.

```bash
$ docker exec -ti mysqldb mysql -u root -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.33 MySQL Community Server - GPL

Copyright (c) 2000, 2023, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

### 어플리케이션을 데이터베이스에 연결
위 명령에서는 `mysqldb` 컨테이너에 'mysql' 명령을 전달하여 MySQL 데이터베이스에 로그인했다. CTRL-D를 눌러 MySQL 대화형 터미널을 종료한다.

다음으로 [이미지 빌드](build-images.md#sample-application) 모듈에서 만든 샘플 애플리케이션을 업데이트gks다. Python 앱의 디렉토리 구조를 확인하려면 [Python 어플리케이션 디렉토리 구조](https://docs.docker.com/language/python/build-images/#directory-structure)를 참조하세요.

이제 실행 중인 MySQL이 있으므로 MySQL을 데이터 저장소로 사용하도록 `app.py`를 업데이트해 보겠다. 또한 서버에 몇 가지 경로를 추가한다. 하나는 레코드를 가져오기 위한 경로이고 다른 하나는 데이터베이스와 테이블을 생성하기 위한 경로이다.

```python
import mysql.connector
import json
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, Docker!'

@app.route('/widgets')
def get_widgets():
    mydb = mysql.connector.connect(
        host="mysqldb",
        user="root",
        password="p@ssw0rd1",
        database="inventory"
    )
    cursor = mydb.cursor()


    cursor.execute("SELECT * FROM widgets")

    row_headers=[x[0] for x in cursor.description] #this will extract row headers

    results = cursor.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))

    cursor.close()

    return json.dumps(json_data)

@app.route('/initdb')
def db_init():
    mydb = mysql.connector.connect(
        host="mysqldb",
        user="root",
        password="p@ssw0rd1"
    )
    cursor = mydb.cursor()

    cursor.execute("DROP DATABASE IF EXISTS inventory")
    cursor.execute("CREATE DATABASE inventory")
    cursor.execute("USE inventory")

    cursor.execute("DROP TABLE IF EXISTS widgets")
    cursor.execute("CREATE TABLE widgets (name VARCHAR(255), description VARCHAR(255))")
    cursor.close()

    return 'init database'

if __name__ == "__main__":
    app.run(host ='0.0.0.0')
```

MySQL 모듈을 추가하고 데이터베이스 서버에 연결하도록 코드를 업데이트하여 데이터베이스와 테이블을 만들었다. 위젯을 가져오는 경로도 만들었다. 이제 변경 사항이 포함되도록 이미지를 다시 빌드해야 한다.

먼저 pip를 사용하여 어플리케이션에 `mysql-connector-python` 모듈을 추가해 보겠다.

```bash
$ python -m pip install mysql-connector-python
$ python -m pip freeze | grep mysql-connector-python >> requirements.txt
```

이제 이미지를 구축할 수 있다.

```bash
$ docker build --tag python-docker-dev .
```

이전 Part에서 컨테이너 이름 `rest-server` 또는 포트 8000을 사용하여 실행 중인 컨테이너가 있는 경우, 지금 [중지하고 제거](run-containers.md#stop-start-and-name-containers)하세요.

이제 데이터베이스 네트워크에 컨테이너를 추가한 다음 컨테이너를 실행해 보겠다. 이렇게 하면 컨테이너 이름으로 데이터베이스를 액세스할 수 있다.

```bash
$ docker run \
  --rm -d \
  --network mysqlnet \
  --name rest-server \
  -p 8000:5000 \
  python-docker-dev
```

어플리케이션이 데이터베이스에 연결되어 있고 노트를 추가할 수 있는지 테스트해 보자.

```bash
$ curl http://localhost:8000/initdb
$ curl http://localhost:8000/widgets
```

서비스에서 다음 JSON을 반환받아야 한다.

```
[]
```

## Compose를 사용하여 로컬에서 개발
이 섹션에서는 단일 명령어를 사용하여 python-docker와 MySQL 데이터베이스를 시작하기 위한 [Compose 파일](https://docs.docker.com/compose/)을 생성하겠다.

IDE 또는 텍스트 편집기에서 `python-docker` 디렉터리를 열고 `docker-compose.dev.yml`이라는 이름의 새 파일을 만든다. 다음 명령을 복사하여 파일에 붙여넣는다.

```yml
version: '3.8'

services:
 web:
  build:
   context: .
  ports:
  - 8000:5000
  volumes:
  - ./:/app

 mysqldb:
  image: mysql
  ports:
  - 3306:3306
  environment:
  - MYSQL_ROOT_PASSWORD=p@ssw0rd1
  volumes:
  - mysql:/var/lib/mysql
  - mysql_config:/etc/mysql

volumes:
  mysql:
  mysql_config:
```

이 Compose 파일은 `docker run` 명령에 전달할 모든 매개 변수를 입력할 필요가 없으므로 매우 편리하다. Compose 파일을 사용하면 선언적으로 이를 수행할 수 있다.

컨테이너 내부의 개발 웹 서버에 연결할 수 있도록 포트 8000을 노출한다. 또한 로컬 소스 코드를 실행 중인 컨테이너에 매핑하여 텍스트 편집기에서 변경을 수행하고 컨테이너에서 해당 변경 사항을 가져오도록 한다.

Compose 파일 사용의 또 다른 멋진 기능은 서비스 이름을 사용하도록 서비스 리솔루션(resolution)을 설정할 수 있다는 것이다. 따라서 이제 연결 문자열에 "mysqldb"를 사용할 수 있다. "mysqldb"를 사용하는 이유는 Compose 파일에서 MySQL 서비스 이름을 그렇게 지었기 때문이다.

이 두 서비스에 대한 네트워크를 지정하지 않았다. docker-compose를 사용하면 자동으로 네트워크가 생성되고 서비스를 네트워크에 연결한다. 자세한 내용은 [Networking in Compose](https://docs.docker.com/compose/networking/)를 참조하세요.

이전 섹션에서 실행 중인 컨테이너가 있는 경우 지금 중지한다.

이제 어플리케이션을 시작하고 제대로 실행되는지 확인하려면 다음 명령을 실행한다.

```bash
$ docker compose -f docker-compose.dev.yml up --build
```

`--build` 플래그를 전달하여 Docker가 이미지를 컴파일한 다음 컨테이너를 시작하도록 한다.

이제 API 엔드포인트를 테스트해 보겠다. 새 터미널을 열고 curl 명령을 사용하여 서버에 GET 요청을 한다.

```bash
$ curl http://localhost:8000/initdb
$ curl http://localhost:8000/widgets
```

다음과 같은 응답을 받으실 수 있다.

```
[]
```

데이터베이스가 비어 있기 때문이다.

## 다음 단계
이 모듈에서는 일반적인 명령어와 거의 비슷하게 사용할 수 있는 일반적인 개발 이미지를 만드는 방법을 살펴보았다. 또한 소스 코드를 실행 중인 컨테이너에 매핑하기 위해 Compose 파일을 설정하였다.

다음 모듈에서는 GitHub Actions를 사용하여 CI/CD 파이프라인을 설정하는 방법을 살펴보겠다.