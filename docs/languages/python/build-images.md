## 전제 조건
- 기본 [Docker 개념](../../get-started/overview.md)에 대한 이해
- [Dockerfile 형식](https://docs.docker.com/build/building/packaging/#dockerfile)에 익숙
- 머신에서 [BuildKit 활성화](https://docs.docker.com/build/buildkit/#getting-started)

## 개요
이제 컨테이너와 Docker 플랫폼에 대해 잘 살펴보았으니 첫 번째 이미지를 빌드하는 방법을 살펴보자. 이미지에는 코드 또는 바이너리, 런타임, 종속성, 기타 필요한 파일 시스템 객체 등 어플리케이션을 실행하는 데 필요한 모든 것이 포함된다.

이 튜토리얼을 완료하려면 다음이 필요하다.

- Python 버전 3.8 이상. [Python 다운로드](https://www.python.org/downloads/)
- 로컬에서 실행 중인 Docker. 지침에 따라 [Docker를 다운로드하여 설치](https://docs.docker.com/desktop/)
- 파일을 편집하기 위한 IDE 또는 텍스트 편집기. [Visual Studio Code](https://code.visualstudio.com/Download)를 사용하는 것을 권장

## <a name="sample_application"></a>샘플 어플리케이션
샘플 어플리케이션은 널리 사용되는 Flask 프레임워크를 사용한다.

로컬 머신에 python-docker라는 디렉터리를 만들고 아래 단계에 따라 Python 가상 환경을 활성화하고 Flask를 종속 요소로 설치한 다음 Python 코드 파일을 생성한다.

```bash
cd /path/to/python-docker
python3 -m venv .venv
source .venv/bin/activate
(.venv) $ python3 -m pip install Flask
(.venv) $ python3 -m pip freeze > requirements.txt
(.venv) $ touch app.py
```

간단한 웹 요청을 처리하는 코드를 추가한다. 자주 사용하는 IDE에서 `python-docker` 디렉토리를 열고 `app.py` 파일에 다음 코드를 입력한다.

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, Docker!'
```

## 어플리케리션 테스트
어플리케이션을 시작하고 실행 중인지 확인한다. 터미널을 열고 생성한 작업 디렉토리로 이동한다.

```bash
cd /path/to/python-docker
source .venv/bin/activate
(.venv) $ python3 -m flask run
```

어플리케이션이 작동하는지 테스트하려면 새 브라우저를 열고 http://localhost:5000 으로 이동한다.

서버가 실행 중인 터미널로 다시 전환하면 서버 로그에 다음과 같은 요청이 표시된다. 데이터와 타임스탬프는 컴퓨터마다 다를 수 있다.

```
127.0.0.1 - - [22/Sep/2020 11:07:41] "GET / HTTP/1.1" 200 -
```

## Python을 위한 Dockerfile 만들기
이제 어플리케이션이 실행 중이므로 어플리케이션으로부터 Dockerfile을 생성할 수 있다.

`python-docker` 디렉터리 내에 Dockerfile을 생성하고 어플리케이션에 사용할 기본 이미지를 Docker에 알려주는 명령을 추가한다.

```dockerfile
# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster
```

Docker 이미지는 다른 이미지로부터 상속받을 수 있다. 따라서 자체 기본 이미지를 만드는 대신 Python 어플리케이션을 실행하는 데 필요한 모든 도구와 패키지가 포함된 공식 Python 이미지를 사용할 수 있다

> **Note**:
>
> 나만의 기본 이미지를 만드는 방법에 대해 자세히 알아보려면 [Creating base images](https://docs.docker.com/build/building/base-images/)를 참조하세요.

나머지 명령을 실행할 때 조금 더 쉽게 작업할 수 있도록 작업 디렉터리를 생성한다. 이렇게 하면 Docker가 이 경로를 이후의 모든 명령에 대하여 기본 위치로 사용하도록 지시하는 것이다. 즉, 전체 파일 경로 대신 작업 디렉터리를 기준으로 상대 파일 경로를 사용할 수 있다.

```dockerfile
WORKDIR /app
```

통상적으로 Python으로 작성된 프로젝트에서 가장 먼저 하는 일은 어플리케이션을 위한 모든 종속성을 설치할 수 있도록 `pip` 패키지를 설치하는 것이다.

`pip` 설치를 실행하기 전에 이미지에 `requirements.txt` 파일이 필요하다. 이를 위해 `COPY` 명령을 사용한다.

`COPY` 명령에는 두 개의 매개변수가 필요하다. 첫 번째 매개 변수는 이미지에 복사할 파일을 Docker에 알려준다. 두 번째 매개변수는 해당 파일을 복사할 위치를 Docker에 알려준다. 이 예에서는 `requirements.txt` 파일을 작업 디렉터리 `/app`에 복사한다.

```dockerfile
COPY requirements.txt requirements.txt
```

이미지 내부에 `requirements.txt` 파일이 있으면 `RUN` 명령을 사용하여 `pip install`를 실행할 수 있다. 이것은 머신에서 로컬에 `pip install`을 실행하는 것과 완전히 동일하게 작동하지만 이번에는 `pip`이 모듈을 이미지에 설치하는 것이다.

```dockerfile
RUN pip install -r requirements.txt
```

이 시점에서 Python 버전 3.8을 기반으로 하는 이미지가 있고 종속성을 설치했다. 다음 단계는 이미지에 소스 코드를 추가하는 것이다. `requirements.txt` 파일과 마찬가지로 `COPY` 명령을 사용한다. 이 `COPY` 명령은 현재 디렉터리에 있는 모든 파일을 가져와서 이미지에 복사한다.

```dockerfile
COPY . .
```

이제 `CMD` 명령을 사용하여 컨테이너 내부에서 이미지가 실행될 때 어떤 명령을 실행할지 Docker에 알려준다. `host=0.0.0.0`을 지정하여 어플리케이션을 외부에서(즉, 컨테이너 외부에서) 볼 수 있도록 해야 한다는 점에 유의하세요.

```dockerfile
CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

전체 Dockerfile은 다음과 같다.

```dockerfile
# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

### 디렉토리 구조
간단히 요약하자면, 로컬 머신에 `python-docker`라는 디렉터리를 만들고 Flask 프레임워크를 사용하여 간단한 Python 어플리케이션을 만들었다. 요구 사항을 수집하기 위해 `requirements.txt` 파일을 사용하고 이미지를 빌드하기 위한 명령이 포함된 Dockerfile을 만들었다. 이제 Python 어플리케이션 디렉토리 구조는 다음과 같을 것이다.

```
python-docker
|____ app.py
|____ requirements.txt
|____ Dockerfile
```

## 이미지 빌드
이제 Dockerfile을 만들었으니 이미지를 빌드해 보자. 이를 위해 `docker build` 명령을 사용한다. `docker build` 명령은 Dockerfile과 "컨텍스트"로 부터 Docker 이미지를 빌드한다. 빌드의 컨텍스트는 지정된 PATH 또는 URL에 있는 파일 집합이다. Docker 빌드 프로세스는 이 컨텍스트에 있는 모든 파일에 액세스할 수 있다.

빌드 명령은 선택적으로 `--tag` 플래그를 사용할 수 있다. 이 태그는 이미지의 이름과 선택적 태그를 `name:tag` 형식으로 설정한다. 작업을 단순화하기 위해 지금은 선택적 `tag`를 생략한다. 태그를 전달하지 않으면 Docker는 "latest"을 기본 태그로 사용한다.

Docker 이미지를 빌드한다.

```bash
docker build --tag python-docker .
[+] Building 2.7s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 203B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/python:3.8-slim-buster
 => [1/6] FROM docker.io/library/python:3.8-slim-buster
 => [internal] load build context
 => => transferring context: 953B
 => CACHED [2/6] WORKDIR /app
 => [3/6] COPY requirements.txt requirements.txt
 => [4/6] RUN pip3 install -r requirements.txt
 => [5/6] COPY . .
 => [6/6] CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]
 => exporting to image
 => => exporting layers
 => => writing image sha256:8cae92a8fbd6d091ce687b71b31252056944b09760438905b726625831564c4c
 => => naming to docker.io/library/python-docker
```

## 로컬 이미지 보기
로컬 머신에 있는 이미지 목록을 보려면 두 가지 옵션이 있다. 하나는 Docker CLI를 사용하는 것이고 다른 하나는 Docker 데스크톱을 사용하는 것이다. 이미 터미널에서 작업하고 있으므로 CLI를 사용하여 이미지를 나열하는 방법을 살펴보.

이미지를 나열하려면 `docker images` 명령을 실행한다.

```bash
$ docker images
REPOSITORY      TAG               IMAGE ID       CREATED         SIZE
python-docker   latest            8cae92a8fbd6   3 minutes ago   123MB
```

방금 빌드한 `python-docker:latest`를 포함하여 하나 이상의 이미지가 나열되어 있어야 한다.

## 이미지 태그하기
앞서 언급했듯이 이미지 이름은 슬래시로 구분된 이름들로 구성된다. 이름 구성 요소에는 소문자, 숫자, 구분 기호가 포함될 수 있다. 구분 기호에는 마침표, 밑줄 하나 또는 두 개, 대시 하나 이상이 포함될 수 있다. 이름 구성 요소는 구분 기호로 시작하거나 구분 기호로 끝날 수 없다.

이미지는 매니페스트와 레이어 목록으로 구성된다. '태그'가 이러한 아티팩트의 조합을 가리키는 것 외에는 이 시점에서 매니페스트와 레이어에 대해 너무 걱정하지 마세요. 이미지에 여러 개의 태그를 사용할 수 있다. 작성한 이미지에 대한 두 번째 태그를 만들고 해당 레이어를 살펴보겠다.

작성한 이미지에 대한 새 태그를 만들려면 다음 명령을 실행한다.

```bash
$ docker tag python-docker:latest python-docker:v1.0.0
```

`docker tag` 명령은 이미지에 대한 새 태그를 만든다. 새 이미지를 만들지는 않는다. 태그는 동일한 이미지를 가리키며 이미지를 참조하는 또 다른 방법일 뿐이다.

이제 `docker images` 명령을 실행하여 로컬 이미지 목록을 확인한다.

```bash
$ docker images
REPOSITORY      TAG               IMAGE ID       CREATED         SIZE
python-docker   latest            8cae92a8fbd6   4 minutes ago   123MB
python-docker   v1.0.0            8cae92a8fbd6   4 minutes ago   123MB
python          3.8-slim-buster   be5d294735c6   9 days ago      113MB
```

두 이미지가 `python-docker`로 시작하는 것을 볼 수 있다. `IMAGE ID` 열을 살펴보면 두 이미지의 값이 동일하다는 것을 알 수 있으므로 같은 이미지임을 알 수 있다.

방금 만든 태그를 제거하자. 이렇게 하려면 `rmi` 명령을 사용한다. `rmi` 명령은 이미지 제거를 의미한다.

```bash
$ docker rmi python-docker:v1.0.0
Untagged: python-docker:v1.0.0
```

Docker의 응답을 보면 Docker가 이미지를 제거한 것이 아니라 "태그가 제거"된 것임을 알 수 있다. 이를 확인하려면 `docker images` 명령을 실행하면 된다.

```bash
$ docker images
REPOSITORY      TAG               IMAGE ID       CREATED         SIZE
python-docker   latest            8cae92a8fbd6   6 minutes ago   123MB
python          3.8-slim-buster   be5d294735c6   9 days ago      113MB
```

Docker에서 `:v1.0.0`으로 태그가 지정된 이미지를 제거했지만 사용자 컴퓨터에서 `python-docker:latest` 태그를 사용할 수 있다.

## 다음 단계
이 모듈에서는 이후 튜토리얼에 사용되는 Python 어플리케이션의 예을 설정하고, Docker 이미지를 빌드하는 데 사용되는 Dockerfile을 생성하는 방법을 살펴보았다. 또한 이미지에 태그를 지정하고 제거하는 방법도 살펴보았다.

다음 모듈에서는 [이미지를 컨테이너로 실행하는 방법](run-containers.md)을 살펴보겠다.
