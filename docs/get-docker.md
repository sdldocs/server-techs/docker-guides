Docker는 어플리케이션을 개발, 배포 및 실행하기 위한 개방형 플랫폼이다. Docker를 사용하면 어플리케이션과 인프라를 분리하여 소프트웨어를 신속하게 제공할 수 있다. Docker를 사용하면 어플리케이션을 관리하는 것과 동일한 방식으로 인프라를 관리할 수 있다. 코드를 신속하게 배포, 테스트 및 디플로잉하기 위한 Docker의 방법론을 활용하면 코드 작성과 프로덕션 환경에서의 실행 사이의 지연을 크게 줄일 수 있다.

Docker를 다운로드하여 다양한 플랫폼에 설치할 수 있다. 다음 섹션을 참조하여 가장 적합한 설치 방법을 선택한다.

> **Note**: 
>
> 이 편역에서는 Mac을 사용할 것이다. 사용하는 Mac의 사양은 아래와 같다.
> ![platform](images/get-docker/platform.png)

[Mac용 Docker 데스크탑](https://sdldocs.gitlab.io/server-techs/docker-desktop/overview/)

> **Note**:
> 
> Docker 엔진을 설치하는 방법에 대한 정보를 찾고 있다면 [Docker Engine installation overview](https://docs.docker.com/engine/install/)에서 찾을 수 있다.
