# Docker Guides
Docker 입문을 위하여 https://docs.docker.com/get-started/overview/ 을 읽으며, 편역하며 가능한 실습 기록을 남기기 위한 것이다. 이는 같은 초보자를 위한 것이다.

이 패이지에는 아래와 같이 구성되어 있으며, 필요에 따라 편역을 수행하였다.

- **Docker 개요**: [Docker Overview](https://docs.docker.com/get-started/overview/)를 편역
- **Docker 설치**: [Get Docker](https://docs.docker.com/get-docker/)를 편역
- **시작하기**: [Get Started](https://docs.docker.com/get-started/)를 편역
- **프로그래밍 언어에 따른 지침**: [Language-specific guides](https://docs.docker.com/language/)를 편역

...
